package api.createSelfEmployed.foreign;

import lombok.Data;

@Data
public class ResponseNewSelfEmployedForeignPOJO {
    private String success;
    private String errorMessage;
    private String errorCode;
}