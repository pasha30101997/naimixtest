package api.createSelfEmployed.foreign;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PassportForeignPOJO {
    private String passportNumber;
    private Object passportIssuerCode;
    private String identificationNumber;
    private String passportIssueDate;
    private String passportType;
    private Object passportSeries;
    private String passportIssuedBy;
    private String passportValidToDate;
}