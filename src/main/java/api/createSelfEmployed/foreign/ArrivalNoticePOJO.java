package api.createSelfEmployed.foreign;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ArrivalNoticePOJO {
    private String arrivalNoticeRegistrationDate;
    private String arrivalNoticeValidToDate;
    private String arrivalNoticeResidenceAddress;
}