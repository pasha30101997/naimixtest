package api.createSelfEmployed.foreign;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class CreateSelfEmployedForeignPOJO {
    private String lastName;
    private String contractorInn;
    private String gender;
    private String bankCardNumber;
    private String citizenship;
    private String birthDate;
    private MigrationCardInfoPOJO migrationCardInfo;
    private List<String> innBindings;
    private String firstName;
    private String birthPlace;
    private String contractorEmail;
    private String patronymic;
    private ArrivalNoticePOJO arrivalNotice;
    private String bankCardValidToDate;
    private PassportForeignPOJO passport;
    private String mainSpecialityId;
    private String contractorPhone;
    private Object residencePermit;
    private List<String> workRegions;
    private Object residenceAddress;
}