package api.createSelfEmployed.foreign;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MigrationCardInfoPOJO {
    private String migrationCardValidToDate;
    private String migrationCardIssueDate;
    private String migrationCardNumber;
    private String migrationCardSeries;
}