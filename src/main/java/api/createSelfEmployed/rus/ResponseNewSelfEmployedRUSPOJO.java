package api.createSelfEmployed.rus;

import lombok.Data;

@Data
public class ResponseNewSelfEmployedRUSPOJO {
    private String success;
    private String errorMessage;
    private String errorCode;
}