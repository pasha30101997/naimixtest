package api.createSelfEmployed.rus;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PassportRUSPOJO {
    private String passportNumber;
    private String passportIssuerCode;
    private String passportIssueDate;
    private String passportSeries;
    private String passportIssuedBy;
}