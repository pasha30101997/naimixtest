package api.createSelfEmployed.rus;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class CreateSelfEmployedRUSPOJO {
    private String lastName;
    private String contractorInn;
    private String gender;
    private String bankCardNumber;
    private String citizenship;
    private String birthDate;
    private List<String> innBindings;
    private String firstName;
    private String contractorEmail;
    private String birthPlace;
    private String patronymic;
    private String bankCardValidToDate;
    private PassportRUSPOJO passport;
    private String mainSpecialityId;
    private String contractorPhone;
    private List<String> workRegions;
    private String residenceAddress;
}