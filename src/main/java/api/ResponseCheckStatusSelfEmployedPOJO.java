package api;

import lombok.Data;

@Data
public class ResponseCheckStatusSelfEmployedPOJO {
    private boolean success;
    private String errorMessage;
    private String errorCode;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String phone;
    private String inn;
    private String status;
}