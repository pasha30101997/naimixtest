package api;

import lombok.Data;

@Data
public class ResponseSigningContractWithSelfEmployedPOJO{
	private boolean success;
	private Object errorMessage;
	private Object errorCode;
}