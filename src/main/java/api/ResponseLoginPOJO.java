package api;

import lombok.Data;

@Data
public class ResponseLoginPOJO {
	private String clientUserId;
	private String role;
	private String clientId;
	private String accessTokenExpirationDateTimeUTC;
	private String loginActive;
	private String errorMessage;
	private String errorCode;
	private String refreshTokenExpirationDateTimeUTC;
	private String accessToken;
	private String refreshToken;
}
