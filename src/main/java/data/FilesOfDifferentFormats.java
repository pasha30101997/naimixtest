package data;

public class FilesOfDifferentFormats {
    public final static String fileFormatDocx = "src/main/resources/testFilesAddContract/testFileAddContract.docx";
    public final static String fileFormatJPG = "src/main/resources/testFilesAddContract/testFileAddContract.jpg";
    public final static String fileFormatODT = "src/main/resources/testFilesAddContract/testFileAddContract.odt";
    public final static String fileFormatPDF = "src/main/resources/testFilesAddContract/testFileAddContract.pdf";
    public final static String fileFormatPNG = "src/main/resources/testFilesAddContract/testFileAddContract.png";
    public final static String fileFormatAVI = "src/main/resources/testFilesAddContract/testFileAddContract.avi";
    public final static String fileFormatTXT = "src/main/resources/testFilesAddContract/testFileAddContract.txt";
    public final static String fileFormatDocSizeMore2Mb = "src/main/resources/testFilesAddContract/testFileAddContact2_8Mb.doc";
    public final static String file2FormatDocx = "src/main/resources/testFilesAddContract/testFileAddContract2.docx";
    public final static String file2FormatJPG = "src/main/resources/testFilesAddContract/testFileAddContract2.jpg";
    public final static String file2FormatODT = "src/main/resources/testFilesAddContract/testFileAddContract2.odt";
    public final static String file2FormatPDF = "src/main/resources/testFilesAddContract/testFileAddContract2.pdf";
    public final static String file2FormatDoc = "src/main/resources/testFilesAddContract/testFileAddContract2.doc";
}