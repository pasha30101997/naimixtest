package elements;

import org.openqa.selenium.By;

public class HomeCompanyForm {
    public By loginLocatorButton = By.cssSelector(".nmx-menu__exit");
    public By companyLeftMenuLocatorButton = By.cssSelector(".nmx-menu__link.nmx-menu__link_active");
    public By addCompanyButton = By.cssSelector(".plus.icon");
    public By addNewCompanyPopUpWindow = By.cssSelector(".Toastify__toast-body");
    public By searchClientField = By.name("nameSubstringFilter");
    public By foundClientString = By.xpath("//tbody/tr/td/a");
    public By searchFioEmployeeField = By.name("clientUserFioFilter");
    public By searchPhoneEmployeeField = By.name("clientUserPhoneFilter");
    public By searchButton = By.cssSelector(".filter-buttons__button-send-text");
    public By dataCompaniesList = By.xpath("//tbody/tr/td");
    public By settingFoundCompanyButton = By.cssSelector(".icon.setting");
    public By archiveFoundCompanyButton = By.cssSelector(".nmx-btn.nmx-btn-default.nmx-btn-sm-size");
}
