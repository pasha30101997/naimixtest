package elements.elementsFormCardCompany;

import org.openqa.selenium.By;

public class StaffOnCardCompanyForm {
    public By addStaffButton = By.cssSelector(".plus.icon");// кнопка добавить сотрудника
    public By lastNameStaff = By.name("lastName");
    public By firstNameStaff = By.name("firstName");
    public By patronymicStaff = By.name("patronymic");
    public By snilsStaff = By.name("snils");
    public By innStaff = By.name("inn");
    public By postStaffDropdownLists = By.xpath("//div[@name='position']/i");
    public By postStaffChoiceOfList = By.xpath("//div[@class='visible menu transition']/div/span");
    public By phoneStaff = By.name("phone");
    public By emailStaff = By.name("email");
    public By roleStaffDropdownLists = By.xpath("//div[@name='role']/i");
    public By roleStaffChoiceOfList = By.xpath("//div[@class='visible menu transition']/div/span");
    public By passwordStaff = By.name("password");
    public By repeatPasswordStaff = By.name("repeatPassword");
    public By addStaffButtonOnForm = By.cssSelector(".text-center");
    public By StaffsButton = By.xpath("//div/a[@class='item ']");
    public By addNewStaffPopUpWindow = By.cssSelector(".Toastify__toast-body");
    public By lengthMore50ValueMessage = By.xpath("//div[text()='Не более 50 символов']");
    public By falseInnMessage = By.xpath("//div[text()='Неправильно введен СНИЛС']");
    public By falseEmailMessage = By.xpath("//div[text()='Не более 320 символов']");
    public By falsePasswordLess6ValueMessage = By.xpath("//div[text()='Пароль не может быть менее 6 символов']");
    public By falsePasswordMore20ValueMessage = By.xpath("//div[text()='Пароль не может быть более 20 символов']");
    public By dataStaffsList = By.xpath("//tbody/tr/td"); //коллекция данных сотрудников
    public By headerStaffsString = By.cssSelector(".nm-title__value.nm-title__value_size-xl"); // строка Сотрудники в заголовке страницы
    public By archiveButton = By.cssSelector(".nm-button-archive ");// кнопка Архив
    public By headerColumnFio = By.cssSelector(".td-container.search-filter-no-active");// заголовок колонки ФИО
    public By SortButton = By.cssSelector(".material-icons.icon-sort.notranslate");// кнопка сортировки рядом С заголовком ФИО
    public By headersColumnsPostRolePhoneEmail = By.xpath("//th"); // заголовки колонок Должность, Номер телефона,Email,Роль
    public By editStaffButton = By.cssSelector(".nmx-icon.nmx-icon_green.material-icons");// кнопка редактирования у сотрудника
    public By archiveStaffButton = By.cssSelector(".nmx-icon.material-icons.notranslate");// кнопка в архив у сотрудника
    public By sortDisplayBy = By.cssSelector(".ui.right.floated.buttons.nmx-size-pag__buttons"); // кнопки сортировки по колличетву записей на странице
    public By pageNumbering = By.cssSelector(".ui.pagination.nmx-pag.menu"); // нумерация страницы
    public By staffsOnStringHeaderNumber = By.cssSelector(".nm-title__count");//колличество сотрудников показанных у заголовка Сотрудники
    public By archiveWindowPopUp = By.cssSelector(".ui.small.modal.transition.visible.active");// окно согласия на добавление сотрудника в архив
    public By yesOnWindowPopUpArchiveButton = By.cssSelector(".ui.primary.button");//кнопка Да на окне подтверждения добавления сотрудника в архив
    public By noOnWindowPopUpArchiveButton = By.xpath("//button[@class='ui button']");//кнопка Нет на окне подтверждения добавления сотрудника в архив
}
