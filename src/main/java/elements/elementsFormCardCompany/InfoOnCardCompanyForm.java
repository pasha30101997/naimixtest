package elements.elementsFormCardCompany;

import org.openqa.selenium.By;

public class InfoOnCardCompanyForm {
    public By optionCompanyListText = By.cssSelector(".label-text__content");
    //public By inputPromoCodeField = By.cssSelector(".field.nm-input_no-active.promocode-card-block__input"); // Удален локатор
    public By bossLabel = By.xpath("//div[@class='card-app__header-title'][text()='Руководитель']"); // label Руководитель
    public By editHeaderButton = By.xpath("//div/i[@aria-hidden='true']");
    public By cancelOnWindowButton = By.xpath("//form/i[text()='clear']");
    public By acceptOnWindowButton = By.xpath("//form/i[text()='check']");
    public By headerCompanyFiend = By.name("name");
    public By errorValueHeaderCompanyFiend = By.cssSelector(".ui.pointing.prompt.label");
    public By headerCompanyString = By.cssSelector(".client-list-header");
    public By addNewCompanyPopUpWindow = By.cssSelector(".Toastify__toast-body");
}

