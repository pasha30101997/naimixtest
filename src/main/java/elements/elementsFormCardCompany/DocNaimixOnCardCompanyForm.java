package elements.elementsFormCardCompany;

import org.openqa.selenium.By;

public class DocNaimixOnCardCompanyForm {
    public By documentsNaimixButton = By.xpath("//div/a[@class='item ']");
    public By editInfoButton = By.cssSelector(".nmx-icon.material-icons.notranslate");
    public By editInfoText = By.cssSelector(".card-app__header-title");
    public By cancelOnWindowButton = By.cssSelector(".nmx-icon.material-icons");
    public By acceptOnWindowButton = By.cssSelector(".check.at.icon");
    public By numberAgencyContractField = By.name("nmContractNumber");
    public By dateAgencyContractString = By.cssSelector(".react-datepicker-ignore-onclickoutside");
    public By dateAgencyContractField = By.xpath("//div/input[@class]");
    public By aboutSuccessfulEditingMessage = By.cssSelector(".Toastify__toast-body");
    public By numberAndDateAgencyContractFields = By.cssSelector(".label-text__content ");
    public By addContractButton = By.cssSelector(".text-app");
    public By addFileButton = By.cssSelector(".ui.primary.button.nmx-btn.nmx-btn-blue-filled");
    public By choiceOrDragText = By.cssSelector(".dropzone-app__title");
    public By formatFileAndMaxSizeText = By.cssSelector(".dropzone-app__limit");
    public By newAgencyContractForm = By.cssSelector(".ui.form.agency-contract-new__form");
    public By saveOnFormAgencyContractButton = By.cssSelector(".ui.button.apply-buttons__submit");
    public By cancelOnFormAgencyContractButton = By.cssSelector(".ui.basic.button.apply-buttons__cancel");
    public By uploadFile = By.cssSelector("[type='file']");
    public By nameFileString = By.cssSelector(".document-element__text");
    public By deleteFileButton = By.cssSelector(".material-icons.document-element__close-icon.document-element__close-icon_gray.notranslate");
    public By currentVersionString = By.cssSelector(".agency-contract-actual__title");
    public By currentVersionFiles = By.cssSelector(".mb-15.app-link");
    public By blockDocumentsInFormCurrentVersionText = By.cssSelector(".agency-contract-actual-file-names");
    public By blockDateInFormCurrentVersionText = By.cssSelector(".column-15.agency-contract-actual-content__col");
    public By blockFIOAdminInFormCurrentVersionText = By.cssSelector(".column-15.agency-contract-history-content__col");
    public By blocksDateAndFioAdminInHistoryListText = By.cssSelector(".column-25.agency-contract-history-content__col");
    public By blockDocumentsInHistoryListText = By.cssSelector(".column-100.agency-contract-history-content__col");
    public By dropDownHistoryButton = By.cssSelector(".material-icons.notranslate.agency-contract-history-header__icon.agency-contract-history-header__icon_color");
}
