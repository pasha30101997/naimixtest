package elements;

import utils.Functions;
import org.openqa.selenium.By;

public class AddCompanyForm {
    public final String abbreviatedNameLegalEntity = "Бамбук" + new Functions().randomNumberFiveValue();
    public By addCompanyButton = By.cssSelector(".ui.button.apply-buttons__submit");
    public By btnCancel = By.cssSelector(".apply-buttons__cancel");
    public By arrowListFaceView = new By.ByXPath("//div[@name='clientType']/i");
    public By choiceViewsLegalFace = new By.ByXPath("//div[@class='visible menu transition']/div");
    public By viewLegalFaceString = new By.ByXPath("//div[@name='clientType']/div[@class='text']");
    public By fullNameString = By.name("fullName");
    public By abbreviatedNameString = By.name("name");
    public By innString = By.name("inn");
    public By addressString = By.className("react-dadata__input");
    public By choiceAddress = new By.ByXPath("//*[./div[@name='address']]/div/span");
    public By saveChoiceAddress = By.className("react-dadata__input");
    public By inputContactPersonName = By.cssSelector("input[name=representativeName]");
    public By inputContactPersonPhone = By.cssSelector("input[name=representativePhone]");
    public By inputContactPersonEmail = By.cssSelector("input[name=representativeEmail]");
    public By inputPromoCode = By.cssSelector(".ui.input");
    public By btnCheckPromoCode = By.xpath("//button[contains(text(), 'Проверить')]");
    public By categoryField = By.name("categoryId");
    public By choiceCategory = new By.ByXPath("//div[@class='visible menu transition']/div/span");
    public By dropDownBtnCategory = By.xpath("//div[./div[contains(text(), 'Категория')]]/.//i");
    public By choiceCategoryString = new By.ByXPath("//*[./div[@name='categoryId']]/div/div[@class='text']");
    public By percentField = By.name("currentCommissionRate");
    public By inputLimit = By.cssSelector("input[name=ordersLimit]");
    public By dropDownSelectedFormOfRegistration = By.xpath("//div[./div[contains(text(), 'Форма')]]/.//div[@class='text']");
    public By flagInsuranceNeeds = By.cssSelector("input[name=insuranceAvailable]");
    public By flagPaymentByRegistries = By.cssSelector("input[name=registryPaymentsAvailable]");
    public By flagOrdersWithoutPledge = By.cssSelector("input[name=ordersUnsecured]");
    //label
    public By labelFormOfRegistration = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'Форма регистрации')]");
    public By labelOfficialName = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'Официальное название')]");
    public By labelShortName = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'Сокращенное название')]");
    public By labelInn = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'ИНН')]");
    public By labelActualAddress = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'Фактический адрес')]");
    public By labelContactPersonName = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'ФИО')]");
    public By labelContactPersonPhone = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'Телефон')]");
    public By labelContactPersonEmail = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'E-mail')]");
    public By labelPromoCode = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'Промо')]");
    public By labelCategory = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'Категория')]");
    public By labelCommission = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'Комиссия')]");
    public By labelInsuranceNeeds = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'страхование')]");
    public By labelPaymentByRegistries = By.xpath("//*[@class='client-new__row-label' and contains(text(), 'реестрами')]");
    public By labelOrdersWithoutPledge = By.xpath("//div[contains(@class, 'client-new__row-label') and contains(text(), 'Заказы без обеспеч')]");
    public By labelLimit = By.cssSelector(".client-new__row-label_no-width");
}
