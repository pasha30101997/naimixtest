package steps.api;

import api.ResponseCheckStatusSelfEmployedPOJO;
import api.ResponseLoginPOJO;
import api.ResponseSigningContractWithSelfEmployedPOJO;
import api.createSelfEmployed.foreign.ResponseNewSelfEmployedForeignPOJO;
import api.createSelfEmployed.rus.ResponseNewSelfEmployedRUSPOJO;
import db.model.ContractorsPOJO;
import io.qameta.allure.Step;
import services.api.RequestSpecificationApi;
import steps.db.DbSteps;
import utils.Builders;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ApiSteps {
    ResponseLoginPOJO loginPOJO;
    public static DbSteps dbSteps = new DbSteps();

    @Step("Авторизация через API")
    public void postLoginInApi() {
        loginPOJO = given()
                .spec(RequestSpecificationApi.REQ_SPEC)
                .body("{\n" +
                        "  \"login\": \"admin@admin.ru\", \n" +
                        "  \"password\": \"Aa123456\"\n" +
                        "}\n")
                .when()
                .post("/auth/login/")
                .then()
                .spec(RequestSpecificationApi.RES_SPEC)
                .log().all()
                .body("errorMessage", equalTo(null))
                .extract().as(ResponseLoginPOJO.class);
    }

    @Step("Проверка полей выходных данных запроса Login")
    public void checkJSONRequestLoginApi() {
        assertThat(loginPOJO.getAccessToken(), notNullValue());
        assertThat(loginPOJO.getRefreshToken(), notNullValue());
        assertThat(loginPOJO.getRole(), notNullValue());
        assertThat(loginPOJO.getClientId(), notNullValue());
        assertThat(loginPOJO.getClientUserId(), notNullValue());
        assertThat(loginPOJO.getErrorMessage(), equalTo(null));
        assertThat(loginPOJO.getErrorCode(), equalTo(null));
        assertThat(loginPOJO.getAccessTokenExpirationDateTimeUTC(), notNullValue());
        assertThat(loginPOJO.getRefreshTokenExpirationDateTimeUTC(), notNullValue());
        assertThat(loginPOJO.getLoginActive(), equalTo(null));
    }

    @Step("Проверка статуса СЗ через API")
    public void getStatusEmployeeApi(String numberPhone) {
        given()
                .spec(RequestSpecificationApi.REQ_SPEC)
                .headers("Authorization", "Bearer " + loginPOJO.getAccessToken())
                .param("phone", numberPhone)
                .when()
                .get("/contractor/status")
                .then()
                .spec(RequestSpecificationApi.RES_SPEC)
                .log().all();
    }

    @Step("Создание исполнителя (рус) и проверка выходного JSON")
    public void createNewSelfEmployedRUS() {
        Builders builders = new Builders();
        given()
                .spec(RequestSpecificationApi.REQ_SPEC)
                .headers("Authorization", "Bearer " + loginPOJO.getAccessToken())
                .body(builders.buildSelfEmployedRUS())
                .when()
                .post("/contractor/init")
                .then()
                .spec(RequestSpecificationApi.RES_SPEC)
                .log().all()
                .body("success", equalTo(true))
                .body("errorCode", nullValue())
                .body("errorMessage", nullValue())
                .extract().as(ResponseNewSelfEmployedRUSPOJO.class);
    }

    @Step("Создание исполнителя (ин) и проверка выходного JSON")
    public void createNewSelfEmployedForeign() {
        Builders builders = new Builders();
        given()
                .spec(RequestSpecificationApi.REQ_SPEC)
                .headers("Authorization", "Bearer " + loginPOJO.getAccessToken())
                .body(builders.buildSelfEmployedForeign())
                .when()
                .post("/contractor/init")
                .then()
                .spec(RequestSpecificationApi.RES_SPEC)
                .log().all()
                .body("success", equalTo(true))
                .body("errorCode", nullValue())
                .body("errorMessage", nullValue())
                .extract().as(ResponseNewSelfEmployedForeignPOJO.class);
    }

    @Step("Подписание договора с СЗ и проверка выходного JSON")
    public void signingContractWithSEAndCheckJson() {
        given()
                .spec(RequestSpecificationApi.REQ_SPEC)
                .headers("Authorization", "Bearer " + loginPOJO.getAccessToken())
                .body("{\n" +
                        "\"contractorInn\": \"825014329127\",\n" +
                        "\"contractorPhone\": \"70009990101\",\n" +
                        "\"contractDate\": \"2020-12-04\",\n" +
                        "\"smsCode\": \"2222\"\n" +
                        "}")
                .when()
                .post("/contract/init/")
                .then()
                .spec(RequestSpecificationApi.RES_SPEC)
                .log().all()
                .body("success", equalTo(true))
                .body("errorCode", nullValue())
                .body("errorMessage", nullValue())
                .extract().as(ResponseSigningContractWithSelfEmployedPOJO.class);
    }

    @Step("Подписание договора с СЗ валидация на некорректное заполнения и проверка выходного JSON")
    public void signingContractWithSEInvalidAndCheckJson() {
        given()
                .spec(RequestSpecificationApi.REQ_SPEC)
                .headers("Authorization", "Bearer " + loginPOJO.getAccessToken())
                .body("{\n" +
                        "  \"contractorInn\": \"825014329127\", \n" +
                        "  \"contractorPhone\": \"700099901XX\",\n" +
                        "  \"contractDate\": \"2020-12-04\",\n" +
                        "  \"smsCode\": \"2222\"\n" +
                        "}")
                .when()
                .post("/contract/init/")
                .then()
                .spec(RequestSpecificationApi.RES_SPEC)
                .log().all()
                .body("success", equalTo(false))
                .body("errorCode", equalTo("INCORRECT_INPUT_ERROR"))
                .body("errorMessage", equalTo("Некорректный формат поля: номер телефона исполнителя"))
                .extract().as(ResponseSigningContractWithSelfEmployedPOJO.class);
    }

    @Step("Подписание договора с СЗ валидация на некорректное заполнения и проверка выходного JSON")
    public void signingContractWithSEBlockAndCheckJson() {
        given()
                .spec(RequestSpecificationApi.REQ_SPEC)
                .headers("Authorization", "Bearer " + loginPOJO.getAccessToken())
                .body("{\n" +
                        "  \"contractorInn\": \"441401690247\", \n" +
                        "  \"contractorPhone\": \"70001010112\",\n" +
                        "  \"contractDate\": \"2020-12-04\",\n" +
                        "  \"smsCode\": \"2222\"\n" +
                        "}")
                .when()
                .post("/contract/init/")
                .then()
                .spec(RequestSpecificationApi.RES_SPEC)
                .log().all()
                .body("success", equalTo(false))
                .body("errorCode", equalTo("CONTRACTOR_BLOCKED"))
                .body("errorMessage", containsString("Исполнитель заблокирован администратором Наймикс"))
                .extract().as(ResponseSigningContractWithSelfEmployedPOJO.class);
    }

    @Step("Проверка статуса самозянятого по телефону и инн")
    public void checkStatusSEonPhoneAndInn() {
        ContractorsPOJO fieldContractor = dbSteps.getPhoneAndInnSelfEmployed();
        given()
                .spec(RequestSpecificationApi.REQ_SPEC)
                .headers("Authorization", "Bearer " + loginPOJO.getAccessToken())
                .param("contractorPhone", fieldContractor.phone)
                .param("contractorInn", fieldContractor.inn)
                .when()
                .get("/tax/checkContractorStatus")
                .then()
                .spec(RequestSpecificationApi.RES_SPEC)
                .log().all();
    }

    @Step("Проверка статуса самозанятого по несуществующему телефону {phone}")
    public void checkStatusSEonNonexistentPhone(String phone) {
        given()
                .spec(RequestSpecificationApi.REQ_SPEC)
                .headers("Authorization", "Bearer " + loginPOJO.getAccessToken())
                .param("contractorPhone", phone)
                .param("contractorInn", "440126218308")
                .when()
                .get("/tax/checkContractorStatus")
                .then()
                .spec(RequestSpecificationApi.RES_SPEC)
                .log().all()
                .body("success", equalTo(false))
                .body("errorCode", equalTo("NO_CONTRACTOR_FOUND"))
                .body("errorMessage", containsString("На платформе Наймикс не найден самозанятый с указанным номером телефона"))
                .extract().as(ResponseCheckStatusSelfEmployedPOJO.class);
    }

    @Step("Проверка статуса самозанятого по несуществующему ИНН {inn}")
    public void checkStatusSEonNonexistentInn(String inn) {
        given()
                .spec(RequestSpecificationApi.REQ_SPEC)
                .headers("Authorization", "Bearer " + loginPOJO.getAccessToken())
                .param("contractorPhone", "70008888801")
                .param("contractorInn", inn)
                .when()
                .get("/tax/checkContractorStatus")
                .then()
                .spec(RequestSpecificationApi.RES_SPEC)
                .log().all()
                .body("errorCode", equalTo("CONTRACTOR_INN_DIFFERS"))
                .body("errorMessage", containsString("На платформе Наймикс ИНН самозанятого отличается от указанного"))
                .extract().as(ResponseCheckStatusSelfEmployedPOJO.class);
    }

    @Step("Проверка статуса самозанятого заблокированного админом Наймикс")
    public void checkStatusSEBlockedAdmin() {
        ContractorsPOJO fieldContractor = dbSteps.getSEBlockedAdmin();
        given()
                .spec(RequestSpecificationApi.REQ_SPEC)
                .headers("Authorization", "Bearer " + loginPOJO.getAccessToken())
                .param("contractorPhone", fieldContractor.phone)
                .param("contractorInn", fieldContractor.inn)
                .when()
                .get("/tax/checkContractorStatus")
                .then()
                .spec(RequestSpecificationApi.RES_SPEC)
                .log().all()
                .body("errorCode", equalTo("CONTRACTOR_BLOCKED"))
                .body("errorMessage", containsString("Исполнитель заблокирован администратором Наймикс"))
                .extract().as(ResponseCheckStatusSelfEmployedPOJO.class);
    }
}
