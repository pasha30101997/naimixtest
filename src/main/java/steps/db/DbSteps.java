package steps.db;

import db.Queries;
import db.model.ClientsPOJO;
import db.model.ContractorsPOJO;
import db.model.ProjectPOJO;
import io.qameta.allure.Step;

import java.util.List;

import static db.Db.query;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;

public class DbSteps {

    @Step("Получить ID и Name таблицы Project, которые не в архиве, в работе, дефолный проект, сумма больше 0")
    public List<ProjectPOJO> getIdAndNameTableProjectWithParams() {
        return query(Queries.class, Queries::nameProjectInWorkEtc);
    }

    @Step("Количество кортежей в полученных данных больше 0")
    public void checkListMoreZero(List<ProjectPOJO> data) {
        assertThat(data.size(), greaterThan(0));
    }

    @Step("Проверить, что компании с {name} не существует")
    public void checkNameClientNotExist(String name) {
        List<ClientsPOJO> clients = query(Queries.class, query -> query.searchClientsOnName(name));
        assertThat("Список найденных имен должен быть пустым", clients.size(), equalTo(0));
    }

    @Step("Получить номер телефона и инн самозанятого")
    public ContractorsPOJO getPhoneAndInnSelfEmployed() {
        assertThat(query(Queries.class, Queries::searchPhoneAndInnSelfEmployed).size(),
                greaterThan(0));
        return query(Queries.class, query -> query.searchPhoneAndInnSelfEmployed().get(0));
    }

    @Step("Получить самозанятых по несущетсвующему телефону {phone} в БД")
    public void getSEOnNonexistentPhone(String phone) {
        List<ContractorsPOJO> selfEmployed = query(Queries.class, query -> query.searchPhoneSelfEmployed(phone));
        assertThat("Список найденных имен должен быть пустым", selfEmployed.size(), equalTo(0));
    }

    @Step("Получить самозанятых по несуществующему ИНН {inn} в БД")
    public void getSEOnNonexistentInn(String inn) {
        List<ContractorsPOJO> selfEmployed = query(Queries.class, query -> query.searchPhoneSelfEmployed(inn));
        assertThat("Список найденных имен должен быть пустым", selfEmployed.size(), equalTo(0));
    }

    @Step("Получить самозянотого заблокированного админом Наймикса")
    public ContractorsPOJO getSEBlockedAdmin() {
        assertThat(query(Queries.class, Queries::searchSEBlockedAdmin).size(),
                greaterThan(0));
        return query(Queries.class, query -> query.searchSEBlockedAdmin().get(0));
    }
}
