package steps.ui;

import pages.AddCompanyPage;
import utils.Functions;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Condition.textCaseSensitive;
import static com.codeborne.selenide.Selenide.*;

public class AddCompanySteps {

    @Step("Нажатие на кнопку 'Добавить' в форме добавления новой компании")
    public void addCompanyButton() {
        $(new AddCompanyPage().onFormAddCompanyElements().addCompanyButton).scrollIntoView(true);
        $(new AddCompanyPage().onFormAddCompanyElements().addCompanyButton).hover();
        $(new AddCompanyPage().onFormAddCompanyElements().addCompanyButton).click();
    }
    @Step("Нажать кнопку Отмена")
    public void clickCancel() {
        $(new AddCompanyPage().onFormAddCompanyElements().btnCancel).hover();
        $(new AddCompanyPage().onFormAddCompanyElements().btnCancel).click();
    }

    @Step("Выбор Юридического лица из выподающего списка поля 'Форма регистрации бизнеса'")
    public void choiceViewsLegalFace() {
        $(new AddCompanyPage().onFormAddCompanyElements().arrowListFaceView).click();
        $$(new AddCompanyPage().onFormAddCompanyElements().choiceViewsLegalFace).get(0).click();
    }

    @Step("Получение из поля 'Форма регистрации бизнеса' выбранного занчения")
    public String saveChoiceViewsLegalFace() {
        return $(new AddCompanyPage().onFormAddCompanyElements().viewLegalFaceString).getText();
    }

    @Step("Ввод в поле 'Официальное название компании' имя компании {fullName}")
    public void fillFullName(String fullName) {
        $(new AddCompanyPage().onFormAddCompanyElements().fullNameString).setValue(fullName);
    }

    @Step("Получение из поля 'Официальное название компании' имя компании")
    public String saveFullName() {
        return $(new AddCompanyPage().onFormAddCompanyElements().fullNameString).getValue();
    }

    @Step("Ввод в поле 'Сокращенное название компании' сокращенное имя компании")
    public void generateAbbreviatedName() {
        $(new AddCompanyPage().onFormAddCompanyElements().abbreviatedNameString).setValue(new AddCompanyPage().onFormAddCompanyElements().abbreviatedNameLegalEntity);
    }

    @Step("Получение из поля 'Сокращенное название компании' сокращенное имя компании")
    public String saveGenerateAbbreviatedName() {
        return $(new AddCompanyPage().onFormAddCompanyElements().abbreviatedNameString).getValue();
    }

    @Step("Ввод в поле 'ИНН' валидного занчения {inn}")
    public void generateInn(String inn) {
        $(new AddCompanyPage().onFormAddCompanyElements().innString).setValue(inn);
    }

    @Step("Получение из поля 'ИНН' введенного значения")
    public String saveGenerateInn() {
        return $(new AddCompanyPage().onFormAddCompanyElements().innString).getValue();
    }

    @Step("Ввод в поле 'Фактический адрес' значения {address}.Выбор из выпадающего списка уточнения адреса")
    public void fillAndChoiceAddress(String address) {
        $(new AddCompanyPage().onFormAddCompanyElements().addressString).setValue(address);
        $$(new AddCompanyPage().onFormAddCompanyElements().choiceAddress).get(0).click();
        $(new AddCompanyPage().onFormAddCompanyElements().addressString).pressTab();
    }

    @Step("Получить из поля 'Фактический адрес' полное значение адреса")
    public String saveChoiceAddress() {
        return $(new AddCompanyPage().onFormAddCompanyElements().saveChoiceAddress).getValue();
    }

    @Step("Выбор в поле 'Категория' валидного значения")
    public void choiceCategory() {
        $(new AddCompanyPage().onFormAddCompanyElements().dropDownBtnCategory).click();
        $$(new AddCompanyPage().onFormAddCompanyElements().choiceCategory).get(2).click();
    }

    @Step("Получить из поля 'Категория' значение")
    public String saveChoiceCategory() {
        return $(new AddCompanyPage().onFormAddCompanyElements().choiceCategoryString).getText();
    }

    @Step("Ввод в поле 'Комиссия' рандомного значения от 0 до 5")
    public void setFieldPercent() {
        $(new AddCompanyPage().onFormAddCompanyElements().percentField).setValue(Integer.toString(new Functions().randomPercent()));
    }

    @Step("Проверка наличия поля выбора формы регистрации")
    public void creationFormDisplayingFormOfRegistration() {
        $(new AddCompanyPage().onFormAddCompanyElements().labelFormOfRegistration).shouldBe(visible).should(textCaseSensitive("Форма регистрации бизнеса"));
        $(new AddCompanyPage().onFormAddCompanyElements().dropDownSelectedFormOfRegistration).shouldBe(visible);
    }

    @Step("Проверка наличия поля ввода официального имени")
    public void creationFormDisplayingOfficialName() {
        $(new AddCompanyPage().onFormAddCompanyElements().labelOfficialName).shouldBe(visible).shouldHave(textCaseSensitive("Официальное название компании"));
        $(new AddCompanyPage().onFormAddCompanyElements().fullNameString).shouldBe(visible);
    }

    @Step("Проверка наличия поля ввода сокр. имени")
    public void creationFormDisplayingShortName() {
        $(new AddCompanyPage().onFormAddCompanyElements().labelShortName).shouldBe(visible).shouldHave(textCaseSensitive("Сокращенное название компании"));
        $(new AddCompanyPage().onFormAddCompanyElements().abbreviatedNameString).shouldBe(visible);
    }

    @Step("Проверка наличия поля ввода ИНН")
    public void creationFormDisplayingInn() {
        $(new AddCompanyPage().onFormAddCompanyElements().labelInn).shouldBe(visible).shouldHave(textCaseSensitive("ИНН"));
        $(new AddCompanyPage().onFormAddCompanyElements().innString).shouldBe(visible);
    }

    @Step("Проверка наличия поля ввода адреса")
    public void creationFormdDsplayingAddress() {
        $(new AddCompanyPage().onFormAddCompanyElements().labelActualAddress).shouldBe(visible).shouldHave(textCaseSensitive("Фактический адрес"));
        $(new AddCompanyPage().onFormAddCompanyElements().addressString).shouldBe(visible);
    }

    @Step("Проверка наличия поля ввода ФИО представителя")
    public void creationFormDisplayingContactPersonName() {
        $(new AddCompanyPage().onFormAddCompanyElements().labelContactPersonName).shouldBe(visible).shouldHave(textCaseSensitive("ФИО контактного лица"));
        $(new AddCompanyPage().onFormAddCompanyElements().inputContactPersonName).shouldBe(visible);
    }

    @Step("Проверка наличия поля ввода телефона")
    public void creationFormDisplayingPhone() {
        $(new AddCompanyPage().onFormAddCompanyElements().labelContactPersonPhone).shouldBe(visible).shouldHave(textCaseSensitive("Телефон контактного лица"));
        $(new AddCompanyPage().onFormAddCompanyElements().inputContactPersonPhone).shouldBe(visible);
    }

    @Step("Проверка наличия поля ввода E-mail")
    public void creationFormDisplayingEmail() {
        $(new AddCompanyPage().onFormAddCompanyElements().labelContactPersonEmail).shouldBe(visible).shouldHave(textCaseSensitive("E-mail контактного лица"));
        $(new AddCompanyPage().onFormAddCompanyElements().inputContactPersonEmail).shouldBe(visible);
    }

    @Step("Проверка наличия поля ввода промо-кода")
    public void creationFormDisplayingPromo() {
        $(new AddCompanyPage().onFormAddCompanyElements().labelPromoCode).shouldBe(visible).shouldHave(textCaseSensitive("Промо-код"));
        $(new AddCompanyPage().onFormAddCompanyElements().inputPromoCode).shouldBe(visible);
        $(new AddCompanyPage().onFormAddCompanyElements().btnCheckPromoCode).shouldBe(visible);
    }

    @Step("Проверка наличия поля ввода категории")
    public void creationFormDisplayingCategory() {
        $(new AddCompanyPage().onFormAddCompanyElements().labelCategory).shouldBe(visible).shouldHave(textCaseSensitive("Категория"));
        $(new AddCompanyPage().onFormAddCompanyElements().dropDownBtnCategory).shouldBe(visible);
    }

    @Step("Проверка наличия поля ввода комиссии")
    public void creationFormDisplayingComission() {
        $(new AddCompanyPage().onFormAddCompanyElements().labelCommission).shouldBe(visible).shouldHave(textCaseSensitive("Комиссия"));
        $(new AddCompanyPage().onFormAddCompanyElements().percentField).shouldBe(visible);
    }

    @Step("Проверка наличия поля ввода лимита")
    public void creationFormDisplayingLimit() {
        $(new AddCompanyPage().onFormAddCompanyElements().labelLimit).shouldBe(visible).shouldHave(textCaseSensitive("Лимит, ₽"));
        $(new AddCompanyPage().onFormAddCompanyElements().inputLimit).shouldBe(visible).shouldBe(disabled);
    }

    @Step("Проверка наличия флага страхования")
    public void creationFormDisplayingInsurance() {
        $(new AddCompanyPage().onFormAddCompanyElements().labelInsuranceNeeds).shouldBe(visible).shouldHave(textCaseSensitive("Необходимое страхование"));
        $(new AddCompanyPage().onFormAddCompanyElements().flagInsuranceNeeds).shouldBe(exist);
    }

    @Step("Проверка наличия флага 'Оплата регистрами'")
    public void creationFormDisplayingPaymentByRegisters() {
        $(new AddCompanyPage().onFormAddCompanyElements().labelPaymentByRegistries).shouldBe(visible).shouldHave(textCaseSensitive("Оплаты реестрами"));
        $(new AddCompanyPage().onFormAddCompanyElements().flagPaymentByRegistries).shouldBe(exist);
    }

    @Step("Проверка наличия 'Заказы без обеспечения'")
    public void creationFormDisplayingOrdersWithoutPledge() {
        $(new AddCompanyPage().onFormAddCompanyElements().flagOrdersWithoutPledge).shouldBe(exist);
        $(new AddCompanyPage().onFormAddCompanyElements().labelOrdersWithoutPledge).shouldBe(visible).shouldHave(textCaseSensitive("Заказы без обеспечения"));
    }

}
