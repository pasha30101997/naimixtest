package steps.ui;

import pages.HomeCompanyPage;
import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class HomeCompanySteps {

    @Step("Нажатие на кнопку выхода с аккаунта")
    public void Logout() {
        $(new HomeCompanyPage().onFormHomeCompanyElements().loginLocatorButton).click();
    }

    @Step("Нажатие на кнопку 'Компании' в левом меню")
    public void clickButtonCompanyLeftMenu() {
        $(new HomeCompanyPage().onFormHomeCompanyElements().companyLeftMenuLocatorButton).click();
    }

    @Step("Нажание на кномпку 'Создать компанию' в правом верхнем углу")
    public void addCompanyButton() {
        $(new HomeCompanyPage().onFormHomeCompanyElements().addCompanyButton).click();
    }

    @Step("Проверка текста в выплывающем окне после соднания компании '{windowAddNewCompany}'")
    public void checkPopUpWindowAddNewCompany(String windowAddNewCompany) {
        $(new HomeCompanyPage().onFormHomeCompanyElements().addNewCompanyPopUpWindow).shouldHave(Condition.text(windowAddNewCompany), Duration.ofSeconds(10));
    }

    @Step("Найти каточку компании в поле 'Заказчик' по сокращенному имени  {nameClient}")
    public void searchClient(String nameClient) {
        $(new HomeCompanyPage().onFormHomeCompanyElements().searchClientField).setValue(nameClient).pressEnter();
        $$(new HomeCompanyPage().onFormHomeCompanyElements().foundClientString).get(0).click();
    }

    @Step("Ввод данных в поля 'Заказчик'= {nameClient}, 'ФИО Сотрудника'= {fioEmployee}, 'Телефон сотрудника' = {phoneEmployee}")
    public void searchClientByDataEmployee(String nameClient, String fioEmployee, String phoneEmployee) {
        $(new HomeCompanyPage().onFormHomeCompanyElements().searchClientField).setValue(nameClient);
        $(new HomeCompanyPage().onFormHomeCompanyElements().searchFioEmployeeField).setValue(fioEmployee);
        $(new HomeCompanyPage().onFormHomeCompanyElements().searchPhoneEmployeeField).click();
        $(new HomeCompanyPage().onFormHomeCompanyElements().searchPhoneEmployeeField).setValue(phoneEmployee);
    }

    @Step("Нажатие на кнопку 'Найти'")
    public void clickButtonSearch() {
        $(new HomeCompanyPage().onFormHomeCompanyElements().searchButton).click();
    }

    @Step("Проверка поиска по введенному значению {nameClient} в поле 'Заказчик'")
    public void checkSearchWithNameCompany(String nameClient) {
        assertThat($$(new HomeCompanyPage().onFormHomeCompanyElements().foundClientString).get(0).getText(),
                containsString(nameClient));
    }

    @Step("Нажатие на найденную компанию после выполнения поиска по фильтру")
    public void clickOnFoundCompany() {
        $$(new HomeCompanyPage().onFormHomeCompanyElements().foundClientString).get(0).click();
    }

    @Step("Проверка видимости Адреса на найденной компании")
    public void checkVisibleAddress() {
        $$(new HomeCompanyPage().onFormHomeCompanyElements().dataCompaniesList).get(2).shouldHave(Condition.visible);
        assertThat($$(new HomeCompanyPage().onFormHomeCompanyElements().dataCompaniesList).get(2).getText(),
                not(emptyString()));
    }

    @Step("Проверка видимости кноки 'Настройки компании' у найденной компании")
    public void checkVisibleButtonSettingFoundCompany() {
        $(new HomeCompanyPage().onFormHomeCompanyElements().settingFoundCompanyButton).shouldHave(Condition.visible);
    }

    @Step("Проверска видимости кномки 'В архив' у найденной компании")
    public void checkVisibleButtonArchiveFoundCompany() {
        $(new HomeCompanyPage().onFormHomeCompanyElements().archiveFoundCompanyButton).shouldHave(Condition.visible);
    }
}
