package steps.ui.сardCompanySteps;

import pages.CardCompanyPage;
import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import java.time.Duration;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class StaffCardCompanySteps {

    @Step("Переход на вкладку 'Сотрудники' и проверка соответствующих элементов на странице")
    public void clickButtonStaffs() {
        $$(new CardCompanyPage().onFormStaffCardCompanyElements().StaffsButton).get(1).click();
        $(new CardCompanyPage().onFormStaffCardCompanyElements().headerStaffsString)
                .shouldHave(Condition.text("Сотрудники"));
        $(new CardCompanyPage().onFormStaffCardCompanyElements().staffsOnStringHeaderNumber)
                .shouldHave(Condition.visible);
        $(new CardCompanyPage().onFormStaffCardCompanyElements().addStaffButton)
                .shouldHave(Condition.visible);
        $(new CardCompanyPage().onFormStaffCardCompanyElements().archiveButton)
                .shouldHave(Condition.visible);
        $(new CardCompanyPage().onFormStaffCardCompanyElements().headerColumnFio)
                .shouldHave(Condition.text("ФИО"));
        $(new CardCompanyPage().onFormStaffCardCompanyElements().SortButton)
                .shouldHave(Condition.visible);
        $$(new CardCompanyPage().onFormStaffCardCompanyElements().headersColumnsPostRolePhoneEmail)
                .get(1)
                .shouldHave(Condition.text("Должность"));
        $$(new CardCompanyPage().onFormStaffCardCompanyElements().headersColumnsPostRolePhoneEmail)
                .get(2)
                .shouldHave(Condition.text("Номер телефона"));
        $$(new CardCompanyPage().onFormStaffCardCompanyElements().headersColumnsPostRolePhoneEmail)
                .get(3)
                .shouldHave(Condition.text("E-mail"));
        $$(new CardCompanyPage().onFormStaffCardCompanyElements().headersColumnsPostRolePhoneEmail)
                .get(4)
                .shouldHave(Condition.text("Роль"));
        $(new CardCompanyPage().onFormStaffCardCompanyElements().editStaffButton)
                .shouldHave(Condition.visible);
        $(new CardCompanyPage().onFormStaffCardCompanyElements().archiveStaffButton)
                .shouldHave(Condition.visible);
        $(new CardCompanyPage().onFormStaffCardCompanyElements().sortDisplayBy)
                .shouldHave(Condition.visible);
        $(new CardCompanyPage().onFormStaffCardCompanyElements().pageNumbering)
                .shouldHave(Condition.visible);
    }

    @Step("Нажатие на кнопку 'Добавить сотрудника'")
    public void clickButtonAddStaff() {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().addStaffButton).click();
    }

    @Step("Заполнение поля 'Фамилия' значением {lastNameStaff}")
    public void setLastNameStaff(String lastNameStaff) {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().lastNameStaff).setValue(lastNameStaff);
    }

    @Step("Не видно сообщения 'Не более 50 символов'")
    public void checkVisibleMessageErrorLengthValueFIO() {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().lengthMore50ValueMessage).should(Condition.not(Condition.visible));
    }

    @Step("Не видно сообщения 'Не более 320 символов'")
    public void checkVisibleMessageErrorLengthValueEmail() {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().falseEmailMessage).should(Condition.not(Condition.visible));
    }

    @Step("Не видно сообщения 'Пароль не может быть менее 6 символов'")
    public void checkVisibleMessageErrorLengthValueLess6Password() {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().falsePasswordLess6ValueMessage).should(Condition.not(Condition.visible));
    }

    @Step("Не видно сообщения 'Пароль не может быть более 20 символов'")
    public void checkVisibleMessageErrorLengthValueMore20Password() {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().falsePasswordMore20ValueMessage).should(Condition.not(Condition.visible));
    }

    @Step("Заполнение поля 'Имя' значением {firstNameStaff}")
    public void setFirstNameStaff(String firstNameStaff) {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().firstNameStaff).setValue(firstNameStaff);
    }

    @Step("Заполнение поля 'Отчество' значением {patronymicStaff}")
    public void setPatronymicStaff(String patronymicStaff) {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().patronymicStaff).setValue(patronymicStaff);
    }

    @Step("Заполнение поля 'СНИЛС' значением {snilsStaff}")
    public void setSnilsStaff(String snilsStaff) {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().snilsStaff).click();
        $(new CardCompanyPage().onFormStaffCardCompanyElements().snilsStaff).setValue(snilsStaff);
    }

    @Step("Заполнение поля 'ИНН' значением {innStaff}")
    public void setInnStaff(String innStaff) {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().innStaff).click();
        $(new CardCompanyPage().onFormStaffCardCompanyElements().innStaff).setValue(innStaff);
    }

    @Step("Выбор значения в поле 'Должность'")
    public void choicePostStaff() {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().postStaffDropdownLists).click();
        $$(new CardCompanyPage().onFormStaffCardCompanyElements().postStaffChoiceOfList).get(2).click();
    }

    @Step("Заполнение поля 'Номер Телефона' значением {phoneStaff}")
    public void setPhoneStaff(String phoneStaff) {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().phoneStaff).click();
        $(new CardCompanyPage().onFormStaffCardCompanyElements().phoneStaff).setValue(phoneStaff);
    }

    @Step("Заполнение поля 'Email' значением {emailStaff}")
    public void setEmailStaff(String emailStaff) {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().emailStaff).setValue(emailStaff);
    }

    @Step("Выбор значения в поле 'Роль'")
    public void choiceRoleStaff() {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().roleStaffDropdownLists).click();
        $$(new CardCompanyPage().onFormStaffCardCompanyElements().roleStaffChoiceOfList).get(2).click();
    }

    @Step("Заполнение поля 'Пароль' значением {passwordStaff}")
    public void setPasswordStaff(String passwordStaff) {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().passwordStaff).setValue(passwordStaff);
    }

    @Step("Заполнение поля 'Поторение пароля' значением {repeatPasswordStaff} ")
    public void setRepeatPasswordStaff(String repeatPasswordStaff) {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().repeatPasswordStaff).setValue(repeatPasswordStaff);
    }

    @Step("Нажатие на кнопку 'Добавить' на форме добавления нового сотрудника")
    public void clickButtonOnFormAddStaff() {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().addStaffButtonOnForm).click();
    }

    @Step("Проверка текста в выплывающем окне после добавления сотрудника '{windowAddNewStaff}'")
    public void checkPopUpWindowAddNewStaff(String windowAddNewStaff) {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().addNewStaffPopUpWindow)
                .shouldHave(Condition.text(windowAddNewStaff), Duration.ofSeconds(10))
                .click();
    }

    @Step("Заполнение поля 'Фамилия' не валидным значением {lastNameStaff} и проверка предупреждающего сообщения")
    public void setInvalidValueLastNameStaff(String lastNameStaff) {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().lastNameStaff).setValue(lastNameStaff);
        $(new CardCompanyPage().onFormStaffCardCompanyElements().addStaffButtonOnForm).click();
        $(new CardCompanyPage().onFormStaffCardCompanyElements().lengthMore50ValueMessage)
                .shouldHave(Condition.text("Не более 50 символов"), Duration.ofSeconds(10));
    }

    @Step("Заполнения поля 'Имя' не валидным значением {firstNameStaff} и проверка предупреждающего сообщения")
    public void setInvalidValueFirstNameStaff(String firstNameStaff) {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().firstNameStaff).setValue(firstNameStaff);
        $(new CardCompanyPage().onFormStaffCardCompanyElements().addStaffButtonOnForm).click();
        $(new CardCompanyPage().onFormStaffCardCompanyElements().lengthMore50ValueMessage)
                .shouldHave(Condition.text("Не более 50 символов"), Duration.ofSeconds(10));
    }

    @Step("Заполнение поля 'Отчество' не валидным значением {patronymicStaff} и проверка предупреждающего сообщения")
    public void setInvalidValuePatronymicStaff(String patronymicStaff) {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().patronymicStaff).setValue(patronymicStaff);
        $(new CardCompanyPage().onFormStaffCardCompanyElements().addStaffButtonOnForm).click();
        $(new CardCompanyPage().onFormStaffCardCompanyElements().lengthMore50ValueMessage)
                .shouldHave(Condition.text("Не более 50 символов"), Duration.ofSeconds(10));
    }

    @Step("Заполнение поля 'СНИЛС' не валидным значением {snilsStaff}")
    public void setInvalidValueSnilsStaff(String snilsStaff) {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().snilsStaff).click();
        $(new CardCompanyPage().onFormStaffCardCompanyElements().snilsStaff).setValue(snilsStaff);
        $(new CardCompanyPage().onFormStaffCardCompanyElements().addStaffButtonOnForm).click();
        $(new CardCompanyPage().onFormStaffCardCompanyElements().falseInnMessage).shouldHave(Condition.text("Неправильно введен СНИЛС"), Duration.ofSeconds(10));
    }

    @Step("Проверка поиска по введенному значению {fioEmployee} в поле 'ФИО Сотрудника' с ФИО")
    public void checkSearchWithFIO(String fioEmployee) {
        assertThat($$(new CardCompanyPage().onFormStaffCardCompanyElements().dataStaffsList).get(0).getText(),
                containsString(fioEmployee));
    }

    @Step("Проверка поиска по введенному значению {phoneNumber} в поле 'Номер телефона' с Телефоном сотрудника")
    public void checkSearchWithNumberPhone(String phoneNumber) {
        String cutPhone = $$(new CardCompanyPage().onFormStaffCardCompanyElements().dataStaffsList)
                .get(2)
                .getText()
                .replaceAll("[^0-9]", "");
        String newPhoneNumber = phoneNumber.replaceAll("[^0-9]", "");
        assertThat(cutPhone, containsString(newPhoneNumber));
    }

    @Step("Нажатие на кнопку Архивировать и проверка всплывающего окна")
    public void clickButtonArchiveAtStringStaff(int n) {
        $$(new CardCompanyPage().onFormStaffCardCompanyElements().archiveStaffButton).get(n).click();
        $(new CardCompanyPage().onFormStaffCardCompanyElements().archiveWindowPopUp)
                .shouldHave(Condition.visible);
        $(new CardCompanyPage().onFormStaffCardCompanyElements().yesOnWindowPopUpArchiveButton)
                .shouldHave(Condition.visible);
        $(new CardCompanyPage().onFormStaffCardCompanyElements().noOnWindowPopUpArchiveButton)
                .shouldHave(Condition.visible);
    }

    @Step("Получение значвения из поля Email")
    public String getEmailStaff(int n) {
        return $$(new CardCompanyPage().onFormStaffCardCompanyElements().dataStaffsList)
                .get(n + 3)
                .getText();
    }

    @Step("Нажатие на модальном окне архивации сотрудника кнопки Нет и проверка оставшегося сотрудника")
    public void clickButtonNoOnModalWindow(String emailStaff, int n) {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().noOnWindowPopUpArchiveButton).click();
        $(new CardCompanyPage().onFormStaffCardCompanyElements().archiveWindowPopUp)
                .shouldHave(Condition.not(Condition.visible));
        $$(new CardCompanyPage().onFormStaffCardCompanyElements().dataStaffsList)
                .get(n + 3)
                .shouldHave(Condition.text(emailStaff));
    }

    @Step("Нажатие на модальном окне архивации сотрудника кнопки Да и проверка оставшегося сотрудника")
    public void clickButtonYesOnModalWindow(String emailStaff) {
        $(new CardCompanyPage().onFormStaffCardCompanyElements().yesOnWindowPopUpArchiveButton).click();
        $(new CardCompanyPage().onFormStaffCardCompanyElements().archiveWindowPopUp)
                .shouldHave(Condition.not(Condition.visible));
        $$(new CardCompanyPage().onFormStaffCardCompanyElements().dataStaffsList)
                .get(3)
                .shouldHave(Condition.text(emailStaff));
        $(new CardCompanyPage().onFormStaffCardCompanyElements().addNewStaffPopUpWindow)
                .shouldHave(Condition.text("Ошибка! У сотрудника есть открытые заказы"))
                .click();
    }
}
