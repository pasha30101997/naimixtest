package steps.ui.сardCompanySteps;

import pages.CardCompanyPage;
import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import java.io.File;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class DocNaimixCardCompanySteps {

    @Step("Нажатие на кнопку верхнего меня 'Документы Наймикс'")
    public void clickButtonDocumentsNaimix() {
        $$(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().documentsNaimixButton).get(0).click();
    }

    @Step("Нажатие на кнопку редактировать и проверить надпись 'Редактирование информации'")
    public void clickButtonEditInfoAndCheckString() {
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().editInfoButton).click();
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().editInfoText).shouldHave(Condition.text("Редактирование информации"));
    }

    @Step("Проверка видимости кнопок 'Крестик' и 'Галочка'")
    public void checkVisibleButtonCancelAndAccept() {
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().cancelOnWindowButton).shouldHave(Condition.visible);
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().acceptOnWindowButton).shouldHave(Condition.visible);
    }

    @Step("Проверка полей 'Номер агенского договора' и 'Дата агенского договора' на доступность к редактированию")
    public void checkFieldsOnEditing() {
        assertThat($(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().numberAgencyContractField).setValue("123456").getValue(),
                containsString("123456"));
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().numberAgencyContractField).sendKeys(Keys.CONTROL + "a");
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().numberAgencyContractField).sendKeys(Keys.DELETE);
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().dateAgencyContractField).click();
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().dateAgencyContractString).sendKeys(Keys.CONTROL + "a");
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().dateAgencyContractString).sendKeys(Keys.DELETE);
        assertThat($(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().dateAgencyContractString).setValue("12.01.2021").getValue(),
                containsString("12.01.2021"));
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().dateAgencyContractField).click();
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().dateAgencyContractString).sendKeys(Keys.CONTROL + "a");
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().dateAgencyContractString).sendKeys(Keys.DELETE);
    }

    @Step("Заполнить полe 'Номер агенского договора' значением {numberAgencyContract}")
    public String setValueInFieldNumberAgencyContract(String numberAgencyContract) {
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().numberAgencyContractField).click();
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().numberAgencyContractField).setValue(numberAgencyContract);
        return $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().numberAgencyContractField).getValue();
    }

    @Step("Заполнить полe 'Дата агенского договора' значением {dateAgencyContract}")
    public String setValueInFieldDateAgencyContract(String dateAgencyContract) {
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().dateAgencyContractField).click();
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().dateAgencyContractString).setValue(dateAgencyContract);
        return $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().dateAgencyContractString).getValue();
    }

    @Step("Нажатие кнопки 'Галочка'")
    public void clickButtonAccept() {
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().acceptOnWindowButton).click();
    }

    @Step("Проверка выплывающего сообщения 'Информация успешно отредактирована'")
    public void checkMessageAboutSuccessfulEditing() {
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().aboutSuccessfulEditingMessage)
                .shouldHave(Condition.text("Информация успешно отредактирована"));
    }

    @Step("Проверка введенных значений в поле 'Номер агенского договора' и 'Дата агенского договора': {valueNumberAgencyContract},{valueDateAgencyContract}")
    public void checkValueNumberAgencyContractAndDateAgencyContract(String valueNumberAgencyContract, String valueDateAgencyContract) {
        assertThat($$(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().numberAndDateAgencyContractFields)
                        .get(0).getText(),
                containsString(valueNumberAgencyContract));
        assertThat($$(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().numberAndDateAgencyContractFields)
                        .get(1).getText(),
                containsString(valueDateAgencyContract));
    }

    @Step("Нажатие на кнопку 'Добавить договор'")
    public void clickButtonAddContract() {
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().addContractButton).click();
    }

    @Step("Нажатие на кнопку 'Добавить договор', проверка наличия формы, текста и функциональности кнопок")
    public void clickButtonAddContractAndCheckForm() {
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().addContractButton).click();
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().newAgencyContractForm).shouldHave(Condition.visible);
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().addFileButton).shouldHave(Condition.visible);
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().choiceOrDragText).shouldHave(Condition.visible);
        $$(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().formatFileAndMaxSizeText).get(0).shouldHave(Condition.visible);
        $$(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().formatFileAndMaxSizeText).get(1).shouldHave(Condition.visible);
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().saveOnFormAgencyContractButton).click();
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().aboutSuccessfulEditingMessage).shouldHave(Condition.text("Файлы не выбраны")).click();
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().cancelOnFormAgencyContractButton).click();
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().newAgencyContractForm).shouldHave(Condition.not(Condition.visible));
    }

    @Step("Загрузка файла и его проверка наличия на форме0")
    public void uploadFileAddForm(String file) {
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().uploadFile).uploadFile(new File(file));
        $$(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().nameFileString)
                .filter(Condition.exactTextCaseSensitive(file.substring(file.lastIndexOf("/") + 1)))
                .shouldHave(CollectionCondition.size(1));
    }

    @Step("Загрузка не валидных файлов и проверка ошибки")
    public void uploadFileAddFormNotValidate(String fileNotValidate) {
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().uploadFile).uploadFile(new File(fileNotValidate));
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().aboutSuccessfulEditingMessage)
                .shouldHave(Condition.text("Неверное расширение файла")).click();
    }

    @Step("Загрузка не валидных файлов размером больше 2 Мб и проверка ошибки")
    public void uploadFileSizeMore2Mb(String fileNotValidate) {
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().uploadFile).uploadFile(new File(fileNotValidate));
/*        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().messageAboutSuccessfulEditing)
                .shouldHave(Condition.text("Неверное расширение файла")).click();*/
    }

    @Step("Удаление файла и его проверка на форме")
    public void deleteFileOnFormAddAgencyContract() {
        String nameDelete = $$(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().nameFileString).get(0).getText();
        $$(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().deleteFileButton).get(0).click();
        $$(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().nameFileString)
                .filter(Condition.exactTextCaseSensitive(nameDelete))
                .shouldHave(CollectionCondition.size(0));
    }

    @Step("Проверка на повторную загрузку файла")
    public void checkOnDoubleFileInDownload(String file) {
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().uploadFile).uploadFile(new File(file));
        $$(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().nameFileString)
                .filter(Condition.exactTextCaseSensitive(file.substring(file.lastIndexOf("/") + 1)))
                .shouldHave(CollectionCondition.size(1));
        String message = "Не удалось загрузить файл " + file.substring(file.lastIndexOf("/") + 1) +
                ". Файл с таким именем уже существует";
        $$(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().formatFileAndMaxSizeText)
                .get(1)
                .shouldHave(Condition.text(message));
    }

    @Step("Проверка на загрузку больше 10 файлов")
    public void checkUploadMoreTenFile(String file) {
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().uploadFile).uploadFile(new File(file));
        $$(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().nameFileString)
                .filter(Condition.exactTextCaseSensitive(file.substring(file.lastIndexOf("/") + 1)))
                .shouldHave(CollectionCondition.size(0));
        $$(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().formatFileAndMaxSizeText)
                .get(1)
                .shouldHave(Condition.text("Версия агентского договора не может содержать более 10 файлов."));
    }

    @Step("Получение данных актуальной версии до добавления новой")
    public String getDataCurrentVersion() {
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().dropDownHistoryButton).scrollIntoView(true);
        String date = $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().blockDateInFormCurrentVersionText).getText();
        String fio = $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().blockFIOAdminInFormCurrentVersionText).getText();
        String nameDocuments = $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().blockDocumentsInFormCurrentVersionText).getText();
        return date + fio + nameDocuments;
    }

    @Step("Сохранение загруженных файлов и проверка Актуальной версии")
    public void clickSaveUploadsFilesAndCheckVisibleForm() {
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().saveOnFormAgencyContractButton).click();
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().currentVersionString)
                .shouldHave(Condition.visible);
        $$(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().currentVersionFiles).get(0)
                .shouldHave(Condition.text("testFileAddContract.jpg"));
        $$(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().currentVersionFiles).get(1)
                .shouldHave(Condition.text("testFileAddContract.odt"));
        $$(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().currentVersionFiles).get(2)
                .shouldHave(Condition.text("testFileAddContract.pdf"));
        $$(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().currentVersionFiles).get(3)
                .shouldHave(Condition.text("testFileAddContract.png"));
    }

    @Step("Проверка Истории после добавления Актуальной версии договора")
    public void checkBlockHistory(String dataCurrentVersion) {
        $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().dropDownHistoryButton).click();
        $$(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().blocksDateAndFioAdminInHistoryListText).get(3).scrollIntoView(true);
        String oldDate = $$(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().blocksDateAndFioAdminInHistoryListText).get(0).getText();
        String oldFIO = $$(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().blocksDateAndFioAdminInHistoryListText).get(1).getText();
        String oldNameDocuments = $(new CardCompanyPage().onFormDocumentsNaimixCardCompanyElements().blockDocumentsInHistoryListText).getText();
        assertThat(oldDate + oldFIO + oldNameDocuments, equalTo(dataCurrentVersion));
    }
}
