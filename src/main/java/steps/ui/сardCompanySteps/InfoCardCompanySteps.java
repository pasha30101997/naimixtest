package steps.ui.сardCompanySteps;

import pages.CardCompanyPage;
import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class InfoCardCompanySteps {

    @Step("Проверка значения 'Официальное название' на карточке компании с значением {fullName}")
    public void checkFullName(String fullName) {
        $$(new CardCompanyPage().onFormInfoCardCompanyElements().optionCompanyListText).get(0).shouldHave(Condition.text(fullName));
    }

    @Step("Проверка значения 'Форма регистрации бизнеса' на карточке компании с значением {setViewLegalFace}")
    public void checkViewLegalFace(String setViewLegalFace) {
        $$(new CardCompanyPage().onFormInfoCardCompanyElements().optionCompanyListText).get(1).shouldHave(Condition.text(setViewLegalFace));
    }

    @Step("Проверка значения 'Фактический адрес' на карточке компании с значением {addressLegalEntity}")
    public void checkAddressLegalEntity(String addressLegalEntity) {
        $$(new CardCompanyPage().onFormInfoCardCompanyElements().optionCompanyListText).get(5).shouldHave(Condition.text(addressLegalEntity));
    }

    @Step("Проверка значения 'ИНН' на карточке компании с значением {inn}")
    public void checkInn(String inn) {
        $$(new CardCompanyPage().onFormInfoCardCompanyElements().optionCompanyListText).get(11).shouldHave(Condition.text(inn));
    }

    @Step("Проверка значения 'Категория' на карточке компании с значением {saveCategory}")
    public void checkCategory(String saveCategory) {
        $(new CardCompanyPage().onFormInfoCardCompanyElements().bossLabel).scrollIntoView(true);
        $(new CardCompanyPage().onFormInfoCardCompanyElements().optionCompanyListText).hover();
        $$(new CardCompanyPage().onFormInfoCardCompanyElements().optionCompanyListText).get(17).shouldHave(Condition.text(saveCategory));
    }

    @Step("Нажатие на кнопку редактирования заголовка и проверка входа в режим редактирования")
    public void clickButtonEditHeaderAndCheckEditing() {
        $(new CardCompanyPage().onFormInfoCardCompanyElements().editHeaderButton).click();
        $(new CardCompanyPage().onFormInfoCardCompanyElements().cancelOnWindowButton).shouldHave(Condition.visible);
        $(new CardCompanyPage().onFormInfoCardCompanyElements().acceptOnWindowButton).shouldHave(Condition.visible);
    }

    @Step("Очистить поле заголовка, сохранить и проверить наличие ошибки")
    public void clearFieldHeaderAndSafeAndCheckError() {
        $(new CardCompanyPage().onFormInfoCardCompanyElements().headerCompanyFiend).sendKeys(Keys.CONTROL + "a");
        $(new CardCompanyPage().onFormInfoCardCompanyElements().headerCompanyFiend).sendKeys(Keys.DELETE);
        $(new CardCompanyPage().onFormInfoCardCompanyElements().acceptOnWindowButton).click();
        $(new CardCompanyPage().onFormInfoCardCompanyElements().errorValueHeaderCompanyFiend).shouldHave(Condition.text("Обязательное поле"));
    }

    @Step("Проверка валидации поля заголовка")
    public void checkValidationHeader() {
        $(new CardCompanyPage().onFormInfoCardCompanyElements().headerCompanyFiend).setValue("Бамбук-.?12Bambyk!");
        $(new CardCompanyPage().onFormInfoCardCompanyElements().acceptOnWindowButton).click();
        $(new CardCompanyPage().onFormInfoCardCompanyElements().errorValueHeaderCompanyFiend).shouldHave(Condition.not(Condition.visible));
        $(new CardCompanyPage().onFormInfoCardCompanyElements().addNewCompanyPopUpWindow).click();
        $(new CardCompanyPage().onFormInfoCardCompanyElements().editHeaderButton).click();
        $(new CardCompanyPage().onFormInfoCardCompanyElements().headerCompanyFiend).setValue("Бамбу");
        $(new CardCompanyPage().onFormInfoCardCompanyElements().acceptOnWindowButton).click();
        $(new CardCompanyPage().onFormInfoCardCompanyElements().errorValueHeaderCompanyFiend).shouldHave(Condition.not(Condition.visible));
        $(new CardCompanyPage().onFormInfoCardCompanyElements().addNewCompanyPopUpWindow).click();
        $(new CardCompanyPage().onFormInfoCardCompanyElements().editHeaderButton).click();
        $(new CardCompanyPage().onFormInfoCardCompanyElements().headerCompanyFiend).setValue("dcuozdtoodcklrvbbkwbpnceplbubamwrgfwekdoxlkjx" +
                "riiurgcmgazglkigeevsoujaixmzostbjlwzobeiihnschiwdrahweg");
        $(new CardCompanyPage().onFormInfoCardCompanyElements().acceptOnWindowButton).click();
        $(new CardCompanyPage().onFormInfoCardCompanyElements().errorValueHeaderCompanyFiend).shouldHave(Condition.not(Condition.visible));
        $(new CardCompanyPage().onFormInfoCardCompanyElements().addNewCompanyPopUpWindow).click();
    }

    @Step("Заполнить заголовок значением {nameCompany} и проверить успешный выход из редактирования")
    public void setValueHeaderAndCheckSuccessfulSafe(String nameCompany) {
        $(new CardCompanyPage().onFormInfoCardCompanyElements().headerCompanyFiend).setValue(nameCompany);
        $(new CardCompanyPage().onFormInfoCardCompanyElements().acceptOnWindowButton).click();
        $(new CardCompanyPage().onFormInfoCardCompanyElements().addNewCompanyPopUpWindow).click();
        $(new CardCompanyPage().onFormInfoCardCompanyElements().cancelOnWindowButton).shouldHave(Condition.not(Condition.visible));
        $(new CardCompanyPage().onFormInfoCardCompanyElements().acceptOnWindowButton).shouldHave(Condition.not(Condition.visible));
        String name = $(new CardCompanyPage().onFormInfoCardCompanyElements().headerCompanyString).getText().replace("\ncreate", "");
        assertThat(name, equalTo(nameCompany));
    }

    @Step("Заполнить заголовок новым значением {newNameCompany}, нажать кномпу отмена, изменения не сохранены и проверить успешный выход из редактирования")
    public void setValueHeaderAndClickButtonCancel(String newNameCompany) {
        String oldName = $(new CardCompanyPage().onFormInfoCardCompanyElements().headerCompanyString).getText().replace("\ncreate", "");
        $(new CardCompanyPage().onFormInfoCardCompanyElements().editHeaderButton).click();
        $(new CardCompanyPage().onFormInfoCardCompanyElements().headerCompanyFiend).setValue(newNameCompany);
        $(new CardCompanyPage().onFormInfoCardCompanyElements().cancelOnWindowButton).click();
        $(new CardCompanyPage().onFormInfoCardCompanyElements().cancelOnWindowButton).shouldHave(Condition.not(Condition.visible));
        $(new CardCompanyPage().onFormInfoCardCompanyElements().acceptOnWindowButton).shouldHave(Condition.not(Condition.visible));
        assertThat($(new CardCompanyPage().onFormInfoCardCompanyElements().headerCompanyString).getText().replace("\ncreate", ""),
                equalTo(oldName));
    }
}
