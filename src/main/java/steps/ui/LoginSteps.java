package steps.ui;

import pages.LoginPage;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class LoginSteps {

    @Step("Ввод логина {loginName} и пароля {passwordName} для успешной авторизации на сайте")
    public void Login(String loginName, String passwordName) {
        $(new LoginPage().onFormLoginElements().loginLocator).setValue(loginName);
        $(new LoginPage().onFormLoginElements().passwordLocator).setValue(passwordName).pressEnter();
    }
}
