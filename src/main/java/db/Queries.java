package db;

import db.model.ClientsPOJO;
import db.model.ContractorsPOJO;
import db.model.ProjectPOJO;
import db.model.mappers.ClientsMapper;
import db.model.mappers.ContractorsMapper;
import db.model.mappers.ProjectMapper;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Define;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.stringtemplate4.UseStringTemplateEngine;

import java.util.List;

@LogSqlFactory
public interface Queries {
    @SqlQuery("SELECT p.\"projectId\" , p.\"name\" \n" +
            "FROM \"Project\" p   \n" +
            "WHERE p.archived = false and p.status = 'IN_WORK' " +
            "and p.\"defaultProject\" = true and p.amount > 0")
    @RegisterRowMapper(ProjectMapper.class)
    List<ProjectPOJO> nameProjectInWorkEtc();

    @SqlQuery("select c.\"name\" \n" +
            "from \"Clients\" c \n" +
            "where \"name\" = '<name>'")
    @RegisterRowMapper(ClientsMapper.class)
    @UseStringTemplateEngine
    List<ClientsPOJO> searchClientsOnName(@Define("name") String name);

    @SqlQuery("select c.phone, c.inn\n" +
            "from \"Contractors\" c\n" +
            "where c.phone notnull and c.inn notnull\n" +
            "limit 50")
    @RegisterRowMapper(ContractorsMapper.class)
    List<ContractorsPOJO> searchPhoneAndInnSelfEmployed();

    @SqlQuery("select c.phone\n" +
            "from \"Contractors\" c\n" +
            "where c.phone = '<phone>'")
    @RegisterRowMapper(ContractorsMapper.class)
    @UseStringTemplateEngine
    List<ContractorsPOJO> searchPhoneSelfEmployed(@Define("phone") String phone);

    @SqlQuery("select c.inn\n" +
            "from \"Contractors\" c\n" +
            "where c.inn = '<inn>'")
    @RegisterRowMapper(ContractorsMapper.class)
    @UseStringTemplateEngine
    List<ContractorsPOJO> searchInnSelfEmployed(@Define("inn") String inn);

    @SqlQuery("select phone, inn\n" +
            "from \"Contractors\" c\n" +
            "where c.\"blocked\" = true and phone notnull and inn notnull\n" +
            "limit 50")
    @RegisterRowMapper(ContractorsMapper.class)
    List<ContractorsPOJO> searchSEBlockedAdmin();
}
