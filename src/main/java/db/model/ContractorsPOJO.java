package db.model;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ContractorsPOJO {
    public String phone;
    public String inn;
}
