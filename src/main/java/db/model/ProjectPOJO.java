package db.model;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ProjectPOJO {
    public String projectId;
    public String name;

    @Override
    public String toString() {
        return "{" +
                "projectId='" + projectId + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
