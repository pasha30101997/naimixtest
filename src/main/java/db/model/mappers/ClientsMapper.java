package db.model.mappers;

import db.model.ClientsPOJO;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientsMapper implements RowMapper<ClientsPOJO> {
    public ClientsPOJO map(ResultSet rs, StatementContext ctx) throws SQLException {
        return new ClientsPOJO(rs.getString("name"));
    }
}
