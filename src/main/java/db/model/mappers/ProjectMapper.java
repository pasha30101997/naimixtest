package db.model.mappers;

import db.model.ProjectPOJO;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProjectMapper implements RowMapper<ProjectPOJO> {
    public ProjectPOJO map(ResultSet resultSet, StatementContext context) throws SQLException {
        return new ProjectPOJO(resultSet.getString("projectId"), resultSet.getString("name"));
    }
}
