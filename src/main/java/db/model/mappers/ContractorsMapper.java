package db.model.mappers;

import db.model.ContractorsPOJO;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ContractorsMapper implements RowMapper<ContractorsPOJO> {
    public ContractorsPOJO map(ResultSet resultSet, StatementContext context) throws SQLException {
        return new ContractorsPOJO(resultSet.getString("phone"), resultSet.getString("inn"));
    }
}
