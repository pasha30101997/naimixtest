package utils;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.With;

import java.util.concurrent.ThreadLocalRandom;

@With
@NoArgsConstructor
@AllArgsConstructor
public class Generate {
    private final String latLC = "abcdefghijklmnopqrstuvwxyz";
    private final String latUC = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private final String cyrLC = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
    private final String curUC = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
    private final String dig = "0123456789";
    private boolean latinLowerCase = true;
    private boolean cyrillicLowerCase = false;
    private boolean latinUpperCase = false;
    private boolean cyrillicUpperCase = false;
    private boolean digits = false;
    private String extra = "";

    private String buildCharSet() {
        StringBuilder resultAlphabetBuilder = new StringBuilder();
        if (latinLowerCase) resultAlphabetBuilder.append(latLC);
        if (latinUpperCase) resultAlphabetBuilder.append(latUC);
        if (cyrillicLowerCase) resultAlphabetBuilder.append(cyrLC);
        if (cyrillicUpperCase) resultAlphabetBuilder.append(curUC);
        if (digits) resultAlphabetBuilder.append(dig);
        resultAlphabetBuilder.append(extra);

        return resultAlphabetBuilder.toString();
    }

    public Generate withLatin(boolean latin) {
        this.latinUpperCase = latin;
        this.latinLowerCase = latin;
        return this;
    }

    public static int randomInt(int maxNotIncluded) {
        return ThreadLocalRandom.current().nextInt(maxNotIncluded);
    }

    public String generate(int length) {
        String alphabet = buildCharSet();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < length; i++) {
            result.append(alphabet.charAt(randomInt(alphabet.length())));
        }
        return result.toString();
    }
}
