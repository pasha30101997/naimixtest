package utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import sun.nio.cs.Surrogate;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Functions {

    public int randomNumberFiveValue() {
        Random random = new Random();
        return random.nextInt(99999) + 10000;
    }

    public int randomPercent() {
        Random random = new Random();
        return random.nextInt(5);
    }

    public static String convertObjToString(Object clsObj) {
        //convert object  to string json
        return new Gson().toJson(clsObj, new TypeToken<Object>() {
        }.getType());
    }

    public static String getRandomPhone() {
        String s = "123456789";
        StringBuilder phoneNumber = new StringBuilder();
        for (int i = 0; i < 9; i++) {
            phoneNumber.append(s.charAt(new Random().nextInt(s.length())));
        }
        return "79"+phoneNumber.toString();
    }

    private static int getNthDigit(String number, int n) {
        return Character.getNumericValue(number.charAt(n));
    }

    public static String randomInnPerson(boolean valid11Digit, boolean valid12Digit) {
        String base = new Generate().withLatin(false).withDigits(true).generate(10);
        int n11 = (getNthDigit(base, 0) * 7 +
                getNthDigit(base, 1) * 2 +
                getNthDigit(base, 2) * 4 +
                getNthDigit(base, 3) * 10 +
                getNthDigit(base, 4) * 3 +
                getNthDigit(base, 5) * 5 +
                getNthDigit(base, 6) * 9 +
                getNthDigit(base, 7) * 4 +
                getNthDigit(base, 8) * 6 +
                getNthDigit(base, 9) * 8) % 11 % 10;

        int n12 = (getNthDigit(base, 0) * 3 +
                getNthDigit(base, 1) * 7 +
                getNthDigit(base, 2) * 2 +
                getNthDigit(base, 3) * 4 +
                getNthDigit(base, 4) * 10 +
                getNthDigit(base, 5) * 3 +
                getNthDigit(base, 6) * 5 +
                getNthDigit(base, 7) * 9 +
                getNthDigit(base, 8) * 4 +
                getNthDigit(base, 9) * 6 +
                n11 * 8) % 11 % 10;

        if (!valid11Digit) {
            n11 = (n11 + 1) % 10;
        }
        if (!valid12Digit) {
            n12 = (n12 + 1) % 10;
        }
        return base + n11 + n12;
    }

    public static String randomInnPersonValid() { return randomInnPerson(true, true); }
}
