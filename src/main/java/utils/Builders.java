package utils;

import api.createSelfEmployed.foreign.ArrivalNoticePOJO;
import api.createSelfEmployed.foreign.CreateSelfEmployedForeignPOJO;
import api.createSelfEmployed.foreign.MigrationCardInfoPOJO;
import api.createSelfEmployed.foreign.PassportForeignPOJO;
import api.createSelfEmployed.rus.CreateSelfEmployedRUSPOJO;
import api.createSelfEmployed.rus.PassportRUSPOJO;

import java.util.Collections;

public class Builders {

    public CreateSelfEmployedRUSPOJO buildSelfEmployedRUS(){
        return CreateSelfEmployedRUSPOJO.builder()
                .lastName("Апих")
                .firstName("Апихов")
                .patronymic("Апихович")
                .gender("M")
                .birthDate("2000-12-24")
                .residenceAddress("Где то там, далеко далеко")
                .contractorEmail("apitest-1@test.api")
                .contractorInn(Functions.randomInnPerson(false, true))
                .contractorPhone(Functions.getRandomPhone())
                .birthPlace("Магадан, еду в Магадан Магадан")
                .bankCardNumber("4929509947106001")
                .bankCardValidToDate("2021-11-20")
                .citizenship("RU")
                .passport(PassportRUSPOJO.builder()
                        .passportSeries("1111")
                        .passportNumber("123456")
                        .passportIssueDate("2020-02-11")
                        .passportIssuedBy("Богами олимпа")
                        .passportIssuerCode("666-666")
                        .build())
                .innBindings(Collections.singletonList("2459012775"))
                .mainSpecialityId("6edd5eb7-191a-4b03-8236-4fd147ebff8d")
                .workRegions(Collections.singletonList("ALTAI_AREA"))
                .build();
    }

    public CreateSelfEmployedForeignPOJO buildSelfEmployedForeign(){
        return CreateSelfEmployedForeignPOJO.builder()
                .firstName("Апихов")
                .lastName("АпихКЗ")
                .patronymic("Апихович")
                .gender("F")
                .birthDate("1999-12-31")
                .birthPlace("Магадан, еду в Магадан")
                .contractorEmail("apitest-1@test.api")
                .contractorInn(Functions.randomInnPerson(false, true))
                .contractorPhone(Functions.getRandomPhone())
                .bankCardNumber("4929509947106001")
                .bankCardValidToDate("2021-11-20")
                .citizenship("KZ")
                .passport(PassportForeignPOJO.builder()
                        .identificationNumber("12345678901234")
                        .passportIssueDate("2020-02-11")
                        .passportIssuedBy("Богами олимпа")
                        .passportIssuerCode(null)
                        .passportNumber("KZ1234567")
                        .passportSeries(null)
                        .passportType("INTERNATIONAL_PASSPORT")
                        .passportValidToDate("2022-02-11")
                        .build())
                .migrationCardInfo(MigrationCardInfoPOJO.builder()
                        .migrationCardIssueDate("2020-12-12")
                        .migrationCardNumber("1234567")
                        .migrationCardSeries("1234")
                        .migrationCardValidToDate("2021-12-12")
                        .build())
                .arrivalNotice(ArrivalNoticePOJO.builder()
                        .arrivalNoticeRegistrationDate("2020-12-12")
                        .arrivalNoticeResidenceAddress("Забугром")
                        .arrivalNoticeValidToDate("2021-12-12")
                        .build())
                .innBindings(Collections.singletonList("2459012773"))
                .residenceAddress(null)
                .residencePermit(null)
                .mainSpecialityId("6edd5eb7-191a-4b03-8236-4fd147ebff8d")
                .workRegions(Collections.singletonList("KARELIA_REPUBLIC"))
                .build();
    }
}
