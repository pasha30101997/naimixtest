package services.api;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class RequestSpecificationApi {

    public static final RequestSpecification REQ_SPEC =
            new RequestSpecBuilder()
                    .setBaseUri("https://nm-test.mmtr.ru/nmapi")
                    .setContentType(ContentType.JSON)
                    .build();
    public static ResponseSpecification RES_SPEC =
            new ResponseSpecBuilder()
                    .expectStatusCode(200)
                    .build();
}
