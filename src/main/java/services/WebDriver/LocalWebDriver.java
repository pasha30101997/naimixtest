package services.WebDriver;

import com.codeborne.selenide.WebDriverProvider;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class LocalWebDriver implements WebDriverProvider {

    public LocalWebDriver() {

    }

    @Nonnull
    @Override
    public WebDriver createDriver(@Nonnull DesiredCapabilities desiredCapabilities) {
        HashMap<String, Object> Prefs = new HashMap<>();
        Prefs.put("profile.default_content_setting_values.notifications", 2);
        Prefs.put("profile.default_content_setting.popups", 0);
        Prefs.put("disable-popups-blocking", true);
        ChromeOptions options = new ChromeOptions();
        List<String> args = new ArrayList<>();
        args.add("disable-translate");
        args.add("disable-plugins");
        args.add("disable-extensions");
        args.add("disable-web-security");
        args.add("disable-infobars");
        args.add("disable-logging");
        args.add("no-default-browser-check");
        args.add("no-sandbox");
        args.add("no-first-run");
        args.add("silent");
        options.addArguments(args);
        options.setExperimentalOption("prefs", Prefs);
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(0L, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(60L, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(120L, TimeUnit.SECONDS);
        return driver;
    }
}
