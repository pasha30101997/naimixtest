package services;

import org.aeonbits.owner.Config;

import static org.aeonbits.owner.Config.*;

@LoadPolicy(LoadType.MERGE)
@Sources({"file:src/main/resources/TestsConfig.test.properties",
        "system:properties"})
public interface TestsConfig extends Config {
    @Key("baseUrl")
    String baseUrl();

    @Key("dbConnectionString")
    String dbConnectionString();

    @Key("users.db.login")
    String dbLogin();

    @Key("users.db.password")
    String dbPassword();

    @Key("dbSchema")
    String dbSchema();

    @Key("driverType")
    @DefaultValue("local")
    String driverType();

    @Key("selenoidUri")
    String selenoidUri();

    @Key("browser")
    @DefaultValue("chrome")
    String browser();
}
