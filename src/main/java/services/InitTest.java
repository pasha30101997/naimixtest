package services;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import org.aeonbits.owner.ConfigFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import services.WebDriver.LocalWebDriver;
import services.WebDriver.RemoteWebDriverProvider;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static com.codeborne.selenide.Selenide.open;

public class InitTest {
    protected static volatile TestsConfig testsConfig;

    @BeforeClass(alwaysRun = true)
    public void startTest() {
        testsConfig = ConfigFactory.create(TestsConfig.class);
        Configuration.browser = testsConfig.driverType().equals("remote") ?
                RemoteWebDriverProvider.class.getCanonicalName() :
                LocalWebDriver.class.getCanonicalName();
        Configuration.baseUrl = testsConfig.baseUrl();

        open("");
        WebDriverRunner.getWebDriver().manage().window().maximize();
    }

    @AfterClass(alwaysRun = true)
    public void close() {
    }
}
