package pages;

import elements.elementsFormCardCompany.DocNaimixOnCardCompanyForm;
import elements.elementsFormCardCompany.InfoOnCardCompanyForm;
import elements.elementsFormCardCompany.StaffOnCardCompanyForm;

public class CardCompanyPage {
    public InfoOnCardCompanyForm onFormInfoCardCompanyElements() {
        return new InfoOnCardCompanyForm();
    }

    public StaffOnCardCompanyForm onFormStaffCardCompanyElements() {
        return new StaffOnCardCompanyForm();
    }

    public DocNaimixOnCardCompanyForm onFormDocumentsNaimixCardCompanyElements() {
        return new DocNaimixOnCardCompanyForm();
    }
}

