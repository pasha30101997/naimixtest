package uiNaimixAutoTest;

import utils.Functions;
import services.InitTest;
import steps.ui.сardCompanySteps.StaffCardCompanySteps;
import steps.ui.HomeCompanySteps;
import steps.ui.LoginSteps;
import org.testng.annotations.Test;

public class AddStaffCompany_T646 extends InitTest {
    public static LoginSteps loginSteps;
    public static HomeCompanySteps homeCompanySteps;
    public static StaffCardCompanySteps staffCardCompanySteps;

    @Test(description = "Создание нового сотрудника в компанию валидными данными",groups = {"UI"})
    public void addStaffCompany() {
        loginSteps = new LoginSteps();
        homeCompanySteps = new HomeCompanySteps();
        staffCardCompanySteps = new StaffCardCompanySteps();
        loginSteps.Login("nmadmin", "nmadmin123");
        homeCompanySteps.clickButtonCompanyLeftMenu();
        homeCompanySteps.searchClient("Бамбук");
        staffCardCompanySteps.clickButtonStaffs();
        staffCardCompanySteps.clickButtonAddStaff();
        staffCardCompanySteps.setLastNameStaff("Бублик");
        staffCardCompanySteps.setFirstNameStaff("Игорь");
        staffCardCompanySteps.setPatronymicStaff("Николаевич");
        staffCardCompanySteps.setSnilsStaff("46578931349");
        staffCardCompanySteps.setInnStaff("838872145850");
        staffCardCompanySteps.choicePostStaff();
        staffCardCompanySteps.setPhoneStaff("+70000001412");
        staffCardCompanySteps.setEmailStaff("roker" + new Functions().randomNumberFiveValue() + "@mail.ru");
        staffCardCompanySteps.choiceRoleStaff();
        staffCardCompanySteps.setPasswordStaff("Byblic.123");
        staffCardCompanySteps.setRepeatPasswordStaff("Byblic.123");
        staffCardCompanySteps.clickButtonOnFormAddStaff();
        staffCardCompanySteps.checkPopUpWindowAddNewStaff("Сотрудник успешно добавлен");
    }
}
