package uiNaimixAutoTest;

import services.InitTest;
import steps.ui.HomeCompanySteps;
import steps.ui.LoginSteps;
import org.testng.annotations.Test;

public class LogInAndLogOutTest extends InitTest {
    public static LoginSteps loginSteps;
    public static HomeCompanySteps homeCompanySteps;

    @Test(description = "Testing login and logout to the site Naimix",groups = {"UI"})
    public void userLogInAndLogOut() {
        loginSteps = new LoginSteps();
        homeCompanySteps = new HomeCompanySteps();
        loginSteps.Login("nmadmin", "nmadmin123");
        homeCompanySteps.Logout();
    }
}
