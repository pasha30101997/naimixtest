package uiNaimixAutoTest;

import services.InitTest;
import steps.ui.сardCompanySteps.InfoCardCompanySteps;
import steps.ui.HomeCompanySteps;
import steps.ui.LoginSteps;
import org.testng.annotations.Test;

public class ValidationHeaderInfoCardCompany_T1740 extends InitTest {
    public static LoginSteps loginSteps;
    public static HomeCompanySteps homeCompanySteps;
    public static InfoCardCompanySteps infoCardCompanySteps;

    @Test(description = "Проверка валидации заголовка на странице информация карточки компании",groups = {"UI"})
    public void ValidationHeaderInfo() {
        loginSteps = new LoginSteps();
        homeCompanySteps = new HomeCompanySteps();
        infoCardCompanySteps = new InfoCardCompanySteps();
        loginSteps.Login("nmadmin", "nmadmin123");
        homeCompanySteps.clickButtonCompanyLeftMenu();
        homeCompanySteps.searchClient("Бамбук");
        infoCardCompanySteps.clickButtonEditHeaderAndCheckEditing();
        infoCardCompanySteps.clearFieldHeaderAndSafeAndCheckError();
        infoCardCompanySteps.checkValidationHeader();
        infoCardCompanySteps.clickButtonEditHeaderAndCheckEditing();
        infoCardCompanySteps.setValueHeaderAndCheckSuccessfulSafe("Бамбук");
        infoCardCompanySteps.setValueHeaderAndClickButtonCancel("Бамбук10");
    }
}
