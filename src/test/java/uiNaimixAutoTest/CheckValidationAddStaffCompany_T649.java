package uiNaimixAutoTest;

import services.InitTest;
import steps.ui.сardCompanySteps.StaffCardCompanySteps;
import steps.ui.HomeCompanySteps;
import steps.ui.LoginSteps;
import io.qameta.allure.Description;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CheckValidationAddStaffCompany_T649 extends InitTest {
    public static LoginSteps loginSteps;
    public static HomeCompanySteps homeCompanySteps;
    public static StaffCardCompanySteps staffCardCompanySteps;

    @Test(dataProvider = "emailAndPassword",groups = {"Checklist"})
    @Description("Заполнение не валидными данными карточку добавления Сотрудника")
    public void checkFieldsAddStaffCompanyOnNotValidateValue(String emailStaff, String passwordStaff, String repeatPasswordStaff) {
        loginSteps = new LoginSteps();
        homeCompanySteps = new HomeCompanySteps();
        staffCardCompanySteps = new StaffCardCompanySteps();
        loginSteps.Login("nmadmin", "nmadmin123");
        homeCompanySteps.clickButtonCompanyLeftMenu();
        homeCompanySteps.searchClient("Бамбук");
        staffCardCompanySteps.clickButtonStaffs();
        staffCardCompanySteps.clickButtonAddStaff();
        staffCardCompanySteps.setInvalidValueLastNameStaff("GCXLnVjngUOozBhOpHCiqsgQrRUvHpggOJmIXMInxTezOIDjoPv");
        staffCardCompanySteps.setInvalidValueFirstNameStaff("eFaktVDxAPNJiukejUPZMpMylHeXQriZPprgmfcnKflgfyehKKX");
        staffCardCompanySteps.setInvalidValuePatronymicStaff("yZPScLYrnLgVEalmbRDisfCPfTwbhxuaarHxZtSVwpQrxthMqMU");
        staffCardCompanySteps.setInvalidValueSnilsStaff("19260657795");
        staffCardCompanySteps.setPhoneStaff("+7asdsfdsgfd");
        staffCardCompanySteps.setEmailStaff(emailStaff);
        staffCardCompanySteps.setPasswordStaff(passwordStaff);
        staffCardCompanySteps.setRepeatPasswordStaff(repeatPasswordStaff);
        staffCardCompanySteps.clickButtonOnFormAddStaff();
    }

    @Test(groups = {"UI"})
    @Description("Заполнение валидными данными карточку добавления Сотрудника")
    public void checkFieldsAddStaffCompanyOnValidateValue() {
        loginSteps = new LoginSteps();
        homeCompanySteps = new HomeCompanySteps();
        staffCardCompanySteps = new StaffCardCompanySteps();
        loginSteps.Login("nmadmin", "nmadmin123");
        homeCompanySteps.clickButtonCompanyLeftMenu();
        homeCompanySteps.searchClient("Бамбук");
        staffCardCompanySteps.clickButtonStaffs();
        staffCardCompanySteps.clickButtonAddStaff();
        staffCardCompanySteps.setLastNameStaff("NrUikYtXwjxbzoimiJpChPqWkmDaVTVpJsNNqkCdWWrbSSkHtI");
        staffCardCompanySteps.clickButtonOnFormAddStaff();
        staffCardCompanySteps.checkVisibleMessageErrorLengthValueFIO();
        staffCardCompanySteps.setFirstNameStaff("nDVESjIYsbyvczzsAuXFOgjYLjjkbcIZSUvCMZbScesmyjezPt");
        staffCardCompanySteps.clickButtonOnFormAddStaff();
        staffCardCompanySteps.checkVisibleMessageErrorLengthValueFIO();
        staffCardCompanySteps.setPatronymicStaff("gGLUPDYfRiMjIfubgnsytnLwUpduYXFNrQcUbdFIobBejfyQsr");
        staffCardCompanySteps.clickButtonOnFormAddStaff();
        staffCardCompanySteps.checkVisibleMessageErrorLengthValueFIO();
        staffCardCompanySteps.setEmailStaff("notlongmainotlongmainotlongmainot" +
                "longmainotlongmainotlongmainotlongmainotlongmainotlongmainotlongmainot" +
                "longmainotlongmainotlongmainotlongmainotlongmainotlongmainotlongmainotlongmai" +
                "notlongmainotlongmainotlongmainotlongmainotlongmainotlongmainotlong@illo.io");
        staffCardCompanySteps.clickButtonOnFormAddStaff();
        staffCardCompanySteps.checkVisibleMessageErrorLengthValueEmail();
        staffCardCompanySteps.setPasswordStaff("Qwer12");
        staffCardCompanySteps.clickButtonOnFormAddStaff();
        staffCardCompanySteps.checkVisibleMessageErrorLengthValueLess6Password();
        staffCardCompanySteps.setPasswordStaff("Qwerty1234Qwerty1234");
        staffCardCompanySteps.clickButtonOnFormAddStaff();
        staffCardCompanySteps.checkVisibleMessageErrorLengthValueMore20Password();
        staffCardCompanySteps.clickButtonOnFormAddStaff();
    }

    @DataProvider(name = "emailAndPassword")
    public static Object[][] dataProviderEmailAndPassword() {
        return new Object[][]{{null, "Qw123", "Qw123"}, {"name@domainio", "Qwerty123456789Qwerty123456789", "Qwerty123456789Qwerty123456789"},
                {"namedomain.io", "Qwer12", "Qwer12"}, {"@domain.io", "QWERTY123", "QWERTY123"},
                {"name@", "Byblic.123", "Byblic.12"}, {"    name@domain.io", null, null}, {"name@domain.io    ", null, null},
                {"longmaillongmaillongmaillongmaillongmaillongmaillongmaillongmaillongmaill" +
                        "ongmaillongmaillongmaillongmaillongmaillongmaillongmaillongmaillongmail" +
                        "longmaillongmaillongmaillongmaillongmaillongmaillongmaillongmaillongmaillongmail" +
                        "longmaillongmaillongmaillongmaillongmaillongmaillongmaillongmaillongmaillongmaillo" +
                        "ngmail@longmail.io", null, null}
        };
    }
}
