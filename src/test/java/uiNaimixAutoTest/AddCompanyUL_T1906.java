package uiNaimixAutoTest;

import services.InitTest;
import steps.ui.AddCompanySteps;
import steps.ui.сardCompanySteps.InfoCardCompanySteps;
import steps.ui.HomeCompanySteps;
import steps.ui.LoginSteps;
import org.testng.annotations.Test;

public class AddCompanyUL_T1906 extends InitTest {
    public static LoginSteps loginSteps;
    public static HomeCompanySteps homeCompanySteps;
    public static AddCompanySteps addCompanySteps;
    public static InfoCardCompanySteps infoCardCompanyStep;

    @Test(description = "We are testing the creation of a new company and checking this card.",groups = {"UI"})
    public void addCompanyUL() {
        loginSteps = new LoginSteps();
        homeCompanySteps = new HomeCompanySteps();
        addCompanySteps = new AddCompanySteps();
        infoCardCompanyStep = new InfoCardCompanySteps();
        loginSteps.Login("nmadmin", "nmadmin123");
        homeCompanySteps.clickButtonCompanyLeftMenu();
        homeCompanySteps.addCompanyButton();
        addCompanySteps.addCompanyButton();
        addCompanySteps.choiceViewsLegalFace();
        String setViewLegalFace = addCompanySteps.saveChoiceViewsLegalFace();
        addCompanySteps.fillFullName("ИП Бамбук");
        String setFullName = addCompanySteps.saveFullName();
        addCompanySteps.generateAbbreviatedName();
        String cutNameLegalEntity = addCompanySteps.saveGenerateAbbreviatedName();
        addCompanySteps.generateInn("4786811667");
        String setInn = addCompanySteps.saveGenerateInn();
        addCompanySteps.fillAndChoiceAddress("г.Кострома, ул.Советская 120");
        String setAddressLegalEntity = addCompanySteps.saveChoiceAddress();
        addCompanySteps.choiceCategory();
        String setCategory = addCompanySteps.saveChoiceCategory();
        addCompanySteps.setFieldPercent();
        addCompanySteps.addCompanyButton();
        homeCompanySteps.checkPopUpWindowAddNewCompany("Компания успешно добавлена");
        homeCompanySteps.clickButtonCompanyLeftMenu();
        homeCompanySteps.searchClient(cutNameLegalEntity);
        infoCardCompanyStep.checkFullName(setFullName);
        infoCardCompanyStep.checkViewLegalFace(setViewLegalFace);
        infoCardCompanyStep.checkAddressLegalEntity(setAddressLegalEntity);
        infoCardCompanyStep.checkInn(setInn);
        infoCardCompanyStep.checkCategory(setCategory);
    }
}
