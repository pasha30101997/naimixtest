package uiNaimixAutoTest;

import services.InitTest;
import steps.ui.сardCompanySteps.StaffCardCompanySteps;
import steps.ui.HomeCompanySteps;
import steps.ui.LoginSteps;
import org.testng.annotations.Test;

public class SearchFilterCompany_T5114 extends InitTest {
    public static LoginSteps loginSteps;
    public static HomeCompanySteps homeCompanySteps;
    public static StaffCardCompanySteps staffCardCompanySteps;

    @Test(description = "Проверка поиска по входным параметрам",groups = {"UI"})
    public void SearchCompanyByEmployeeData() {
        loginSteps = new LoginSteps();
        homeCompanySteps = new HomeCompanySteps();
        staffCardCompanySteps = new StaffCardCompanySteps();
        loginSteps.Login("nmadmin", "nmadmin123");
        homeCompanySteps.clickButtonCompanyLeftMenu();
        homeCompanySteps.searchClientByDataEmployee("Бамбук", "Пупкин", "+70000007475");
        homeCompanySteps.clickButtonSearch();
        homeCompanySteps.checkSearchWithNameCompany("Бамбук");
        homeCompanySteps.clickOnFoundCompany();
        staffCardCompanySteps.clickButtonStaffs();
        staffCardCompanySteps.checkSearchWithFIO("Пупкин");
        staffCardCompanySteps.checkSearchWithNumberPhone("+70000007475");
    }
}
