package uiNaimixAutoTest;

import services.InitTest;
import steps.ui.сardCompanySteps.StaffCardCompanySteps;
import steps.ui.HomeCompanySteps;
import steps.ui.LoginSteps;
import org.testng.annotations.Test;

public class SearchOnNumberPhoneStaff_T5117 extends InitTest {
    public static LoginSteps loginSteps;
    public static HomeCompanySteps homeCompanySteps;
    public static StaffCardCompanySteps staffCardCompanySteps;

    @Test(description = "Проверка строки компании по поиску с входными данными",groups = {"UI"})
    public void SearchCompanyByEmployeeData() {
        loginSteps = new LoginSteps();
        homeCompanySteps = new HomeCompanySteps();
        staffCardCompanySteps = new StaffCardCompanySteps();
        loginSteps.Login("nmadmin", "nmadmin123");
        homeCompanySteps.clickButtonCompanyLeftMenu();
        homeCompanySteps.searchClientByDataEmployee("Тракторист", "Опасный Павел Иванович", "+7(159)159-15-91");
        homeCompanySteps.clickButtonSearch();
        homeCompanySteps.checkSearchWithNameCompany("Тракторист");
        homeCompanySteps.checkVisibleAddress();
        homeCompanySteps.checkVisibleButtonSettingFoundCompany();
        homeCompanySteps.checkVisibleButtonArchiveFoundCompany();
    }
}
