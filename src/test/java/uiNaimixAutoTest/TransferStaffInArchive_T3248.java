package uiNaimixAutoTest;

import services.InitTest;
import steps.ui.сardCompanySteps.StaffCardCompanySteps;
import steps.ui.HomeCompanySteps;
import steps.ui.LoginSteps;
import org.testng.annotations.Test;

public class TransferStaffInArchive_T3248 extends InitTest {
    public static LoginSteps loginSteps;
    public static HomeCompanySteps homeCompanySteps;
    public static StaffCardCompanySteps staffCardCompanySteps;

    @Test(description = "Добавление сотрудника в архив",groups = {"UI"})
    public void transferStaffInArchive() {
        loginSteps = new LoginSteps();
        homeCompanySteps = new HomeCompanySteps();
        staffCardCompanySteps = new StaffCardCompanySteps();
        loginSteps.Login("nmadmin", "nmadmin123");
        homeCompanySteps.clickButtonCompanyLeftMenu();
        homeCompanySteps.searchClient("Бамбук");
        staffCardCompanySteps.clickButtonStaffs();
        String emailStaff = staffCardCompanySteps.getEmailStaff(0);
        staffCardCompanySteps.clickButtonArchiveAtStringStaff(0);
        staffCardCompanySteps.clickButtonNoOnModalWindow(emailStaff, 0);
        staffCardCompanySteps.clickButtonArchiveAtStringStaff(0);
        staffCardCompanySteps.clickButtonYesOnModalWindow(emailStaff);
        staffCardCompanySteps.clickButtonArchiveAtStringStaff(1);
        staffCardCompanySteps.clickButtonYesOnModalWindow(emailStaff);
    }
}
