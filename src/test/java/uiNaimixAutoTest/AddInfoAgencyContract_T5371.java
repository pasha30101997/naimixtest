package uiNaimixAutoTest;

import services.InitTest;
import steps.ui.сardCompanySteps.DocNaimixCardCompanySteps;
import steps.ui.HomeCompanySteps;
import steps.ui.LoginSteps;
import org.testng.annotations.Test;

public class AddInfoAgencyContract_T5371 extends InitTest {
    public static LoginSteps loginSteps;
    public static HomeCompanySteps homeCompanySteps;
    public static DocNaimixCardCompanySteps docNaimixCardCompanySteps;

    @Test(description = "Добавление новой информации в разделе Документы Наймикс",groups = {"UI"})
    public void addInfoAgencyContract() {
        loginSteps = new LoginSteps();
        homeCompanySteps = new HomeCompanySteps();
        docNaimixCardCompanySteps = new DocNaimixCardCompanySteps();
        loginSteps.Login("nmadmin", "nmadmin123");
        homeCompanySteps.clickButtonCompanyLeftMenu();
        homeCompanySteps.searchClient("Бамбук");
        docNaimixCardCompanySteps.clickButtonDocumentsNaimix();
        docNaimixCardCompanySteps.clickButtonEditInfoAndCheckString();
        docNaimixCardCompanySteps.checkVisibleButtonCancelAndAccept();
        docNaimixCardCompanySteps.checkFieldsOnEditing();
        String valueNumberAgencyContract = docNaimixCardCompanySteps.setValueInFieldNumberAgencyContract("123456789");
        String valueDateAgencyContract = docNaimixCardCompanySteps.setValueInFieldDateAgencyContract("20.03.2021");
        docNaimixCardCompanySteps.clickButtonAccept();
        docNaimixCardCompanySteps.checkMessageAboutSuccessfulEditing();
        docNaimixCardCompanySteps.checkValueNumberAgencyContractAndDateAgencyContract
                (valueNumberAgencyContract, valueDateAgencyContract);
    }
}
