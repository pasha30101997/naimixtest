package uiNaimixAutoTest;

import data.FilesOfDifferentFormats;
import services.InitTest;
import steps.ui.сardCompanySteps.DocNaimixCardCompanySteps;
import steps.ui.HomeCompanySteps;
import steps.ui.LoginSteps;
import org.testng.annotations.Test;

public class SendNewAgencyContract_T1292 extends InitTest {
    public static LoginSteps loginSteps;
    public static HomeCompanySteps homeCompanySteps;
    public static DocNaimixCardCompanySteps docNaimixCardCompanySteps;

    @Test(description = "Добавление новых документов в разделе Агенский договор на странице Документы Наймикс",
            groups = {"UI"})
    public void addNewFilesInAgencyContract() {
        loginSteps = new LoginSteps();
        homeCompanySteps = new HomeCompanySteps();
        docNaimixCardCompanySteps = new DocNaimixCardCompanySteps();
        loginSteps.Login("nmadmin", "nmadmin123");
        homeCompanySteps.clickButtonCompanyLeftMenu();
        homeCompanySteps.searchClient("Бамбук");
        docNaimixCardCompanySteps.clickButtonDocumentsNaimix();
        docNaimixCardCompanySteps.clickButtonAddContractAndCheckForm();
        docNaimixCardCompanySteps.clickButtonAddContract();
        docNaimixCardCompanySteps.uploadFileAddForm(FilesOfDifferentFormats.fileFormatDocx);
        docNaimixCardCompanySteps.uploadFileAddForm(FilesOfDifferentFormats.fileFormatJPG);
        docNaimixCardCompanySteps.uploadFileAddForm(FilesOfDifferentFormats.fileFormatODT);
        docNaimixCardCompanySteps.uploadFileAddForm(FilesOfDifferentFormats.fileFormatPDF);
        docNaimixCardCompanySteps.uploadFileAddForm(FilesOfDifferentFormats.fileFormatPNG);
        docNaimixCardCompanySteps.deleteFileOnFormAddAgencyContract();
        String dataCurrentVersion = docNaimixCardCompanySteps.getDataCurrentVersion();
        docNaimixCardCompanySteps.clickSaveUploadsFilesAndCheckVisibleForm();
        docNaimixCardCompanySteps.checkBlockHistory(dataCurrentVersion);
    }
}