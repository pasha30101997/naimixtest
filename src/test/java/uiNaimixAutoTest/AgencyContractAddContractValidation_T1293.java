package uiNaimixAutoTest;

import data.FilesOfDifferentFormats;
import services.InitTest;
import steps.ui.сardCompanySteps.DocNaimixCardCompanySteps;
import steps.ui.HomeCompanySteps;
import steps.ui.LoginSteps;
import org.testng.annotations.Test;

public class AgencyContractAddContractValidation_T1293 extends InitTest {
    public static LoginSteps loginSteps;
    public static HomeCompanySteps homeCompanySteps;
    public static DocNaimixCardCompanySteps docNaimixCardCompanySteps;

    @Test(description = "Добавление новых документов и проверка валидации в разделе Агенский договор на странице Документы Наймикс",
            groups = {"UI"})
    public void addNewFilesAgencyContractAndCheckValidation() {
        loginSteps = new LoginSteps();
        homeCompanySteps = new HomeCompanySteps();
        docNaimixCardCompanySteps = new DocNaimixCardCompanySteps();
        loginSteps.Login("nmadmin", "nmadmin123");
        homeCompanySteps.clickButtonCompanyLeftMenu();
        homeCompanySteps.searchClient("Бамбук");
        docNaimixCardCompanySteps.clickButtonDocumentsNaimix();
        docNaimixCardCompanySteps.clickButtonAddContractAndCheckForm();
        docNaimixCardCompanySteps.clickButtonAddContract();
        docNaimixCardCompanySteps.uploadFileAddForm(FilesOfDifferentFormats.fileFormatDocx);
        docNaimixCardCompanySteps.uploadFileAddForm(FilesOfDifferentFormats.fileFormatJPG);
        docNaimixCardCompanySteps.uploadFileAddForm(FilesOfDifferentFormats.fileFormatODT);
        docNaimixCardCompanySteps.uploadFileAddForm(FilesOfDifferentFormats.fileFormatPDF);
        docNaimixCardCompanySteps.uploadFileAddForm(FilesOfDifferentFormats.fileFormatPNG);
        docNaimixCardCompanySteps.uploadFileAddFormNotValidate(FilesOfDifferentFormats.fileFormatAVI);
        docNaimixCardCompanySteps.uploadFileAddFormNotValidate(FilesOfDifferentFormats.fileFormatTXT);
        docNaimixCardCompanySteps.uploadFileSizeMore2Mb(FilesOfDifferentFormats.fileFormatDocSizeMore2Mb);
        docNaimixCardCompanySteps.checkOnDoubleFileInDownload(FilesOfDifferentFormats.fileFormatDocSizeMore2Mb);
        docNaimixCardCompanySteps.uploadFileAddForm(FilesOfDifferentFormats.file2FormatDocx);
        docNaimixCardCompanySteps.uploadFileAddForm(FilesOfDifferentFormats.file2FormatJPG);
        docNaimixCardCompanySteps.uploadFileAddForm(FilesOfDifferentFormats.file2FormatODT);
        docNaimixCardCompanySteps.uploadFileAddForm(FilesOfDifferentFormats.file2FormatPDF);
        docNaimixCardCompanySteps.checkUploadMoreTenFile(FilesOfDifferentFormats.file2FormatDoc);
        docNaimixCardCompanySteps.deleteFileOnFormAddAgencyContract();
        String dataCurrentVersion = docNaimixCardCompanySteps.getDataCurrentVersion();
        docNaimixCardCompanySteps.clickSaveUploadsFilesAndCheckVisibleForm();
        docNaimixCardCompanySteps.checkBlockHistory(dataCurrentVersion);
    }
}