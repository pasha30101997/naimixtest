package apiNaimixAutoTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Features;
import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;
import steps.api.ApiSteps;

@Features({@Feature("АПИ"), @Feature("2.2 Подписание договора с СЗ")})
public class SigningContractSelfEmployedApi {
    public static ApiSteps apiSteps;

    @TmsLink("NAMEMIX-T5534")
    @Test(description = "АПИ: Подписание договора с СЗ", groups = {"API"})
    public void signingContractSelfEmployed() {
        apiSteps = new ApiSteps();
        apiSteps.postLoginInApi();
        apiSteps.signingContractWithSEAndCheckJson();
    }

    @TmsLink("NAMEMIX-T6567")
    @Test(description = "АПИ: Подписание договора с СЗ валидация на некорректное заполнения", groups = {"API"})
    public void signingContractSelfEmployedInvalid() {
        apiSteps = new ApiSteps();
        apiSteps.postLoginInApi();
        apiSteps.signingContractWithSEInvalidAndCheckJson();
    }

    @TmsLink("NAMEMIX-T6568")
    @Test(description = "АПИ: Подписание договора с СЗ валидация на некорректное заполнения", groups = {"API"})
    public void signingContractSelfEmployedBlock() {
        apiSteps = new ApiSteps();
        apiSteps.postLoginInApi();
        apiSteps.signingContractWithSEBlockAndCheckJson();
    }
}
