package apiNaimixAutoTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Features;
import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;
import steps.api.ApiSteps;

@Features({@Feature("АПИ"), @Feature("6.1 Добавление самозанятого")})
public class CreateNewSelfEmployedRusApi {
    public static ApiSteps apiSteps;

    @TmsLink("NAMEMIX-T5973")
    @Test(description = "СЗ: Создание исполнителя (рус)", groups = {"API"})
    public void createNewSelfEmployed() {
        apiSteps = new ApiSteps();
        apiSteps.postLoginInApi();
        apiSteps.createNewSelfEmployedRUS();
    }
}
