package apiNaimixAutoTest;

import org.testng.annotations.Test;
import steps.api.ApiSteps;


public class LoginApi {
    public static ApiSteps apiSteps;

    @Test(description = "Провести успешную авторизацию спомощью API",
            groups = {"API"})
    public void Login() {
        apiSteps = new ApiSteps();
        apiSteps.postLoginInApi();
        apiSteps.checkJSONRequestLoginApi();
    }
}