package apiNaimixAutoTest;

import org.testng.annotations.Test;
import steps.api.ApiSteps;

public class StatusEmployeeApi {
    public static ApiSteps apiSteps;

    @Test(description = "Проверка статуса самозанятого",
            groups = {"API"})
    public void statusEmployeeApi() {
        apiSteps = new ApiSteps();
        apiSteps.postLoginInApi();
        apiSteps.getStatusEmployeeApi("70000001111");
    }
}
