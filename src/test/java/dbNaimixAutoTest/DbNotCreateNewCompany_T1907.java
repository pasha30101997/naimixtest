package dbNaimixAutoTest;

import org.testng.annotations.Test;
import services.InitTest;
import steps.db.DbSteps;
import steps.ui.AddCompanySteps;
import steps.ui.HomeCompanySteps;
import steps.ui.LoginSteps;

public class DbNotCreateNewCompany_T1907 extends InitTest {
    public LoginSteps loginSteps;
    public HomeCompanySteps homeCompanySteps;
    public AddCompanySteps addCompanySteps;
    public DbSteps dbSteps;

    @Test(description = "Добавить компанию ЮЛ - валидация.Step.1.Вход с ситему и нажатие на кнопку 'Добавить компанию'",
            groups = {"DB"})
    public void loginAndCreateNewCompany() {
        loginSteps = new LoginSteps();
        homeCompanySteps = new HomeCompanySteps();
        addCompanySteps = new AddCompanySteps();
        dbSteps = new DbSteps();
        loginSteps.Login("nmadmin", "nmadmin123");
        homeCompanySteps.clickButtonCompanyLeftMenu();
        homeCompanySteps.addCompanyButton();
        addCompanySteps.choiceViewsLegalFace();
        addCompanySteps.creationFormDisplayingFormOfRegistration();
        addCompanySteps.creationFormDisplayingOfficialName();
        addCompanySteps.creationFormDisplayingShortName();
        addCompanySteps.creationFormDisplayingInn();
        addCompanySteps.creationFormdDsplayingAddress();
        addCompanySteps.creationFormDisplayingContactPersonName();
        addCompanySteps.creationFormDisplayingPhone();
        addCompanySteps.creationFormDisplayingEmail();
        //addCompanySteps.creationFormDisplayingPromo(); *** Поле пропало с формы***
        addCompanySteps.creationFormDisplayingCategory();
        addCompanySteps.creationFormDisplayingComission();
        addCompanySteps.creationFormDisplayingInsurance();
        addCompanySteps.creationFormDisplayingPaymentByRegisters();
        addCompanySteps.creationFormDisplayingOrdersWithoutPledge();
        addCompanySteps.choiceViewsLegalFace();
        addCompanySteps.fillFullName("ИП Бамбук");
        addCompanySteps.generateAbbreviatedName();
        String cutNameLegalEntity = addCompanySteps.saveGenerateAbbreviatedName();
        addCompanySteps.generateInn("4786811667");
        addCompanySteps.fillAndChoiceAddress("г.Кострома, ул.Советская 120");
        addCompanySteps.choiceCategory();
        addCompanySteps.setFieldPercent();
        addCompanySteps.clickCancel();
        dbSteps.checkNameClientNotExist(cutNameLegalEntity);
    }
}
