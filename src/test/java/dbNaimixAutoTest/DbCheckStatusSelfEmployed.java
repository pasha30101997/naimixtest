package dbNaimixAutoTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Features;
import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;
import steps.api.ApiSteps;
import steps.db.DbSteps;

@Features({@Feature("АПИ"), @Feature("3.1 Проверка статуса СЗ")})
public class DbCheckStatusSelfEmployed {
    public ApiSteps apiSteps;
    public DbSteps dbSteps;

    @TmsLink("NAMEMIX-T6036")
    @Test(description = "Произвести проверку статуса самозанятого", groups = {"DB"})
    public void checkStatusSelfEmployed() {
        apiSteps = new ApiSteps();
        apiSteps.postLoginInApi();
        apiSteps.checkStatusSEonPhoneAndInn();
    }

    @TmsLink("NAMEMIX-T6576")
    @Test(description = "Произвести проверку статуса самозанятого с несуществующим номером", groups = {"DB"})
    public void checkStatusSEWithNonexistentPhone() {
        apiSteps = new ApiSteps();
        dbSteps = new DbSteps();
        apiSteps.postLoginInApi();
        String nonExistentPhone = "79909909090";
        dbSteps.getSEOnNonexistentPhone(nonExistentPhone);
        apiSteps.checkStatusSEonNonexistentPhone(nonExistentPhone);
    }

    @TmsLink("NAMEMIX-T6578")
    @Test(description = "Произвести проверку статуса самозанятого с несуществующим ИНН", groups = {"DB"})
    public void checkStatusSEWithNonexistentInn() {
        apiSteps = new ApiSteps();
        dbSteps = new DbSteps();
        apiSteps.postLoginInApi();
        String nonExistentInn = "440000000000";
        dbSteps.getSEOnNonexistentInn(nonExistentInn);
        apiSteps.checkStatusSEonNonexistentInn(nonExistentInn);
    }

    @TmsLink("NAMEMIX-T6579")
    @Test(description = "Произвести проверку статуса самозанятого заблокированного Наймикс", groups = {"DB"})
    public void checkStatusSEBlockedAdmin() {
        apiSteps = new ApiSteps();
        dbSteps = new DbSteps();
        apiSteps.postLoginInApi();
        dbSteps.getSEBlockedAdmin();
        apiSteps.checkStatusSEBlockedAdmin();
    }
}
