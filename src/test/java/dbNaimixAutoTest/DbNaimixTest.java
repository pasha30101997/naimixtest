package dbNaimixAutoTest;

import org.testng.annotations.Test;
import steps.db.DbSteps;

public class DbNaimixTest {
    public DbSteps dbSteps;

    @Test(description = "Вывести ID и Name таблицы Project, которые не в архиве, в работе, дефолный проект, сумма больше 0",
            groups = {"DB"})
    public void writeConsoleQueries() {
        dbSteps = new DbSteps();
        System.out.println(dbSteps.getIdAndNameTableProjectWithParams());
        dbSteps.checkListMoreZero(dbSteps.getIdAndNameTableProjectWithParams());
    }
}
